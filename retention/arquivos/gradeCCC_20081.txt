UFBA 
-Universidade 
Federal 
da 
Bahia 
-Sistema 
Acad�mico 
15/07/2013 
11:40 
R00041 
-Grade 
Curricular 
(Curso) 
Curso: 
112140 
Curr�culo: 
2008-1 
Turno: 
Diurno 
Dura��o 
em 
anos: 
M�nima 
5 
M�dia 
6 
M�xima 
7 


Ci�ncia da Computa��o 
�rea: 
Matem�tica, 
Ci�ncias 
F�sicas 
e 
Tecnologia 
Titula��o: 
Bacharel 
em 
Ci�ncia 
da 
Computa��o 
Habilita��o: 
Bacharelado 
Base 
Legal: 
AUTORIZA��O: RESOLU��O CONSUNI/UFBA N� 04 DE 22.01.1971. PARECER CFE N� 417/80, APROVADO EM 09.04.1980. 


RECONHECIMENTO: DECRETO N� 82027 DE 24.07.78. PARECER CFE N� 1910/78. RENOVA��O DE RECONHECIMENTO: PORTARIA N� 
291 DE 02.02.2011 


1�SEMESTRECr�dito/Semestre0Horas/Semestre42525Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MATA01 
GEOMETRIA 
ANAL�TICA 
68 
0 
OB 
MATA02 
C�LCULO 
A 
102 
0 
OB 
MATA37 
INTRODU��O 
� 
L�GICA 
DE 
PROGRAMA��O 
68 
0 
OB 
MATA38 
PROJETO 
DE 
CIRCUITOS 
L�GICOS 
68 
0 
OB 
MATA39 
SEMIN�RIOS 
DE 
INTRODU��O 
AO 
CURSO 
51 
0 
OB 
MATA42 
MATEM�TICA 
DISCRETA 
I 
68 
0 
OB 


2�SEMESTRECr�dito/Semestre0Horas/Semestre42525Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 


FCHC45 
METODOLOGIA 
E 
EXPRESS�O 
T�CNICO-CIENT�F 
68 
0 
OB 
MATA07 
�LGEBRA 
LINEAR 
A 
68 
0 
OB 
01 MATA01 
MATA40 
ESTRUTURAS 
DE 
DADOS 
E 
ALGORITMOS 
I 
68 
0 
OB 
01 MATA37 MATA42 
MATA57 
LABORAT�RIO 
DE 
PROGRAMA��O 
I 
51 
0 
OB 
01 MATA37 
MATA95 
COMPLEMENTOS 
DE 
C�LCULO 
102 
0 
OB 
01 MATA01 MATA02 
MATA97 
MATEM�TICA 
DISCRETA 
II 
68 
0 
OB 
01 MATA42 


3�SEMESTRECr�dito/Semestre0Horas/Semestre45927Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
FISA75 
ELEMENTOS 
DO 
ELETROMAGNETISMO 
E 
DE 
CIRC102 
0 
OB 
01 MATA95 
MATA47 
L�GICA 
PARA 
COMPUTA��O 
68 
0 
OB 
01 MATA97 
MATA50 
LINGUAGENS 
FORMAIS 
E 
AUT�MATOS 
68 
0 
OB 
01 MATA42 
MATA55 
PROGRAMA��O 
ORIENTADA 
A 
OBJETOS 
68 
0 
OB 
01 MATA40 
MATA96 
ESTAT�STICA 
A 
102 
0 
OB 
01 MATA42 MATA95 
OPT051 
OPTATIVA 
051 
51 
0 
OP 


4�SEMESTRECr�dito/Semestre0Horas/Semestre32319Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MATA48 
ARQUITETURA 
DE 
COMPUTADORES 
68 
0 
OB 
01 MATA38 
MATA51 
TEORIA 
DA 
COMPUTA��O 
68 
0 
OB 
01 MATA47 
MATA52 
AN�LISE 
E 
PROJETO 
DE 
ALGORITMOS 
68 
0 
OB 
01 MATA40 
MATA62 
ENGENHARIA 
DE 
SOFTWARE 
I 
68 
0 
OB 
01 MATA55 
MATA68 
COMPUTADOR, 
�TICA 
E 
SOCIEDADE 
51 
0 
OB 


5�SEMESTRECr�dito/Semestre0Horas/Semestre39123Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MATA49 
PROGRAMA��O 
DE 
SOFTWARE 
B�SICO 
68 
0 
OB 
01 MATA40 MATA48 MATA57 
MATA53 
TEORIA 
DOS 
GRAFOS 
68 
0 
OB 
01 MATA52 
MATA54 
ESTRUTURAS 
DE 
DADOS 
E 
ALGORITMOS 
II 
68 
0 
OB 
01 MATA52 


MATA56 
PARADIGMAS 
DE 
LINGUAGENS 
DE 
PROGRAMA�� 
68 
0 
OB 
01 MATA55 
MATA63 
ENGENHARIA 
DE 
SOFTWARE 
II 
68 
0 
OB 
01 MATA62 
OPT051 
OPTATIVA 
051 
51 
0 
OP 


6�SEMESTRECr�dito/Semestre0Horas/Semestre39123Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MATA58 
SISTEMAS 
OPERACIONAIS 
68 
0 
OB 
01 MATA49 
MATA59 
REDES 
DE 
COMPUTADORES 
I 
68 
0 
OB 
01 MATA49 
MATA60 
BANCO 
DE 
DADOS 
68 
0 
OB 
01 MATA54 
MATA61 
COMPILADORES 
68 
0 
OB 
01 MATA49 MATA50 
MATA64 
INTELIG�NCIA 
ARTIFICIAL 
68 
0 
OB 
01 MATA47 MATA53 MATA56 
OPT051 
OPTATIVA 
051 
51 
0 
OP 


7�SEMESTRECr�dito/Semestre0Horas/Semestre34020Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MATA65 
COMPUTA��O 
GR�FICA 
68 
0 
OB 
01 MATA07 MATA57 MATA95 
OPT068 
OPTATIVA 
068 
68 
0 
OP 
OPT068 
OPTATIVA 
068 
68 
0 
OP 


P�g. 
1 
de 
4 



UFBA 
-Universidade 
Federal 
da 
Bahia 
-Sistema 
Acad�mico 
15/07/2013 
11:40 
R00041 
-Grade 
Curricular 
(Curso) 


7�SEMESTRECr�dito/Semestre0Horas/Semestre34020Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
OPT068 
OPTATIVA 
068 
68 
0 
OP 
OPT068 
OPTATIVA 
068 
68 
0 
OP 


8�SEMESTRECr�dito/Semestre0Horas/Semestre34020Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
OPT068 
OPTATIVA 
068 
68 
0 
OP 
OPT068 
OPTATIVA 
068 
68 
0 
OP 
OPT068 
OPTATIVA 
068 
68 
0 
OP 
OPT068 
OPTATIVA 
068 
68 
0 
OP 
OPT068 
OPTATIVA 
068 
68 
0 
OP 


9�SEMESTRECr�dito/Semestre0Horas/Semestre39123Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MATA66 
PROJETO 
FINAL 
DE 
CURSO 
I 
51 
0 
OB 
01 FCHC45 
OPT068 
OPTATIVA 
068 
68 
0 
OP 
OPT068 
OPTATIVA 
068 
68 
0 
OP 
OPT068 
OPTATIVA 
068 
68 
0 
OP 
OPT068 
OPTATIVA 
068 
68 
0 
OP 
OPT068 
OPTATIVA 
068 
68 
0 
OP 


10�SEMESTRECr�dito/Semestre0Horas/Semestre27216Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MATA67 
PROJETO 
FINAL 
DE 
CURSO 
II 
136 
0 
OB 
01 MATA66 
OPT068 
OPTATIVA 
068 
68 
0 
OP 
OPT068 
OPTATIVA 
068 
68 
0 
OP 


OPTATIVAS
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
ADM001 
INTRODUCAO 
� 
ADMINISTRACAO 
68 
0 
OP 
ADM011 
PESQUISA 
OPERACIONAL 
68 
0 
OP 
01 ADM001 MATA96 
ADM170 
ADMINISTRACAO 
CONTABIL 
I 
68 
0 
OP 
ADM171 
ELEMENTOS 
E 
ANALISES 
DE 
CUSTOS 
68 
0 
OP 
01 ADM001 
ADM241 
INTRODUCAO 
AO 
MARKETING 
51 
0 
OP 
ADM243 
GER�NCIA 
CONTEMPOR�NEA 
68 
0 
OP 
ECO001 
FUNDAMENTOS 
DE 
ECONOMIA 
51 
0 
OP 


EDC001 
EDUCA��O 
ABERTA, 
CONTINUADA 
E 
� 
DIST�NC 
68 
0 
OP 
ENG229 
APLICA��ES 
INDUSTRIAIS 
DA 
COMPUTA��O 
68 
0 
OP 
ENG336 
ELETR�NICA 
DIGITAL 
68 
0 
OP 
01 MATA38 
ENG646 
AUTOMACAO 
DE 
SISTEMAS 
51 
3 
OP 
01 MATA48 
ENG648 
CONTROLE 
E 
AUTOMACAO 
DE 
PROCESSOS 
51 
3 
OP 
01 MATA07 MATA95 
FCC001 
CONTABILIDADE 
GERAL 
I 
68 
0 
OP 
FISA76 
OSCILA��ES 
E 
ONDAS 
ELETROMAGN�TICAS 
102 
0 
OP 
01 FISA75 
IPSA39 
PSICOLOGIA 
DAS 
RELACOES 
HUMANAS 
68 
0 
OP 
LET358 
INGLES 
INSTRUMENTAL 
III 
N-100 
51 
0 
OP 
LET359 
INGLES 
INSTRUMENTAL 
IV 
N-100 
51 
0 
OP 
01 LET358 
LETE46 
LIBRAS-L�NGUA 
BRASILEIRA 
DE 
SINAIS 
34 
0 
OP 
MAT174 
CALCULO 
NUM�RICO 
I 
68 
0 
OP 
01 MATA07 MATA37 MATA95 
MAT220 
EMPREENDEDORES 
EM 
INFORMATICA 
68 
0 
OP 
MATA04 
C�LCULO 
C 
102 
0 
OP 
MATA05 
CALCULO 
D 
102 
0 
OP 
01 MATA95 
MATA41 
INFORM�TICA 
NA 
EDUCA��O 
68 
0 
OP 
MATA69 
MODELAGEM 
E 
SIMULA��O 
DE 
SISTEMAS 
68 
0 
OP 
01 MATA07 MATA96 
MATA71 
AN�LISE 
NUM�RICA 
68 
0 
OP 
01 MAT174 MATA07 MATA95 
MATA72 
T�PICOS 
EM 
ARQUITETURA 
DE 
COMPUTADORES 
51 
0 
OP 
01 MATA48 
MATA73 
LABORAT�RIO 
DE 
CIRCUITOS 
DIGITAIS 
51 
0 
OP 
01 MATA38 
MATA74 
T�PICOS 
EM 
COMPUTA��O 
E 
ALGORITMOS 
51 
0 
OP 
01 MATA50 MATA51 MATA52 
MATA75 
SEM�NTICA 
DE 
LINGUAGEM 
DE 
PROGRAMA��O 
68 
0 
OP 
01 MATA47 MATA56 
MATA76 
LINGUAGENS 
PARA 
APLICA��O 
COMERCIAL 
51 
0 
OP 
01 MATA40 
MATA77 
PROGRAMA��O 
FUNCIONAL 
68 
0 
OP 
01 MATA40 MATA97 
MATA79 
T�PICOS 
EM 
PROGRAMA��O 
51 
0 
OP 
01 MATA54 MATA56 


P�g. 
2 
de 
4 



UFBA 
-Universidade 
Federal 
da 
Bahia 
-Sistema 
Acad�mico 
15/07/2013 
11:40 
R00041 
-Grade 
Curricular 
(Curso) 


OPTATIVAS
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MATA80 
LABORAT�RIO 
DE 
PROGRAMA��O 
II 
51 
0 
OP 
01 MATA52 
MATA81 
LABORAT�RIO 
DE 
SISTEMAS 
OPERACIONAIS 
51 
0 
OP 
01 MATA58 
MATA82 
SISTEMAS 
DE 
TEMPO 
REAL 
68 
0 
OP 
01 MATA58 
MATA83 
T�PICOS 
EM 
SISTEMAS 
OPERACIONAIS 
51 
0 
OP 
01 MATA58 
MATA85 
REDES 
DE 
COMPUTADORES 
II 
68 
0 
OP 
01 MATA59 
MATA86 
T�PICOS 
EM 
REDES 
DE 
COMPUTADORES 
51 
0 
OP 
01 MATA59 
MATA87 
SEGURAN�A 
DA 
INFORMA��O 
68 
0 
OP 
01 MATA07 
MATA88 
FUNDAMENTOS 
DE 
SISTEMAS 
DISTRIBUIDOS 
51 
0 
OP 
01 MATA58 MATA59 
MATA89 
ARQUITETURAS 
DE 
SISTEMAS 
DISTRIBU�DOS 
51 
0 
OP 
01 MATA58 MATA59 
MATA90 
ALGORITMOS 
DISTRIBUIDOS 
51 
0 
OP 
01 MATA88 
MATB01 
LABORAT�RIO 
DE 
REDES 
DE 
COMPUTADORES 
51 
0 
OP 
01 MATA59 
MATB02 
QUALIDADE 
DE 
SOFTWARE 
51 
0 
OP 
01 MATA63 
MATB03 
EVOLU��O 
DE 
SOFTWARE 
51 
0 
OP 
01 MATA63 
MATB04 
T�PICOS 
EM 
COMPUTA��O 
GR�FICA 
E 
PROCESS 
51 
0 
OP 
01 MATA65 
MATB05 
T�PICOS 
EM 
INTELIG�NCIA 
ARTIFICIAL 
51 
0 
OP 
01 MATA64 
MATB06 
T�PICOS 
EM 
SISTEMAS 
DISTRIBUIDOS 
51 
0 
OP 
01 MATA88 
MATB09 
LABORAT�RIO 
DE 
BANCO 
DE 
DADOS 
51 
0 
OP 
01 MATA60 
MATB10 
T�PICOS 
EM 
BANCO 
DE 
DADOS 
51 
0 
OP 
01 MATA60 
MATB11 
LABORAT�RIO 
DE 
COMPILADORES 
51 
0 
OP 
01 MATA61 
MATB12 
T�PICOS 
EM 
COMPILADORES 
51 
0 
OP 
01 MATA61 
MATB13 
M�TODOS 
FORMAIS 
51 
0 
OP 
01 MATA47 MATA62 
MATB14 
LABORAT�RIO 
DE 
ENGENHARIA 
DE 
SOFTWARE 
51 
0 
OP 
01 MATA63 
MATB15 
VALIDA��O 
DE 
SOFTWARE 
51 
0 
OP 
01 MATA63 
MATB16 
LABORAT�RIO 
DE 
INTELIG�NCIA 
ARTIFICIAL 
51 
0 
OP 
01 MATA64 
MATB17 
LAB. 
DE 
COMPUTA��O 
GR�FICA 
E 
PROCESSAME 
51 
0 
OP 
01 MATA65 
MATB19 
SISTEMAS 
MULTIM�DIA 
68 
0 
OP 
01 MATA55 
MATB20 
INTELIG�NCIA 
ARTIFICIAL 
EM 
EDUCA��O 
68 
0 
OP 
01 MATA41 MATA64 
MATB21 
AMBIENTES 
INTERATIVOS 
DE 
APRENDIZAGEM 
68 
0 
OP 
01 MATA37 MATA41 
MATB22 
LABORAT�RIO 
DE 
INFORM�TICA 
NA 
EDUCA��O 
51 
0 
OP 
01 MATB20 MATB21 
MATB23 
SEMIN�RIOS 
EM 
EMPREENDEDORISMO 
51 
0 
OP 
01 MATB22 
MATB24 
ROB�TICA 
INTELIGENTE 
51 
0 
OP 
01 MATA48 MATA57 MATA64 
MATB25 
T�PICOS 
EM 
ENGENHARIA 
DE 
SOFTWARE 
51 
0 
OP 
01 MATA63 
MATB26 
T�PICOS 
EM 
SISTEMAS 
MULTIM�DIA 
51 
0 
OP 
01 MATB19 
MATB27 
TEORIA 
DAS 
CATEGORIAS 
68 
0 
OP 
01 MATB29 
MATB28 
�LGEBRA 
LINEAR 
B 
68 
0 
OP 
01 MATA07 
MATB29 
L�GICA 
MATEM�TICA 
68 
0 
OP 
01 MATA47 
MATB65 
EMPREENDIMENTOS 
E 
INFORM�TICA 
68 
0 
OP 


INTEGRALIZA��O 
CURRICULAR 


Natureza 
Carga 
Hor�ria 
Credita��o 
Disciplina 
Nome 
M�xima 
M�nima 
M�xima 
M�nima 
AC 
Atividade 
Complementar 
100 
100 
OB 
Obrigatoria 
2516 
2516 
OP 
Optativa 
731 
731 
Total 
3347 
3347 
0 
0 


Observa��o: 
Conforme processo n� 23066.022363/05-42 a carga hor�ria de Atividade Complementar s�o 200 horas, tendo seu valor m�ximo em 204 horas. 


P�g. 
3 
de 
4 



UFBA 
-Universidade 
Federal 
da 
Bahia 
-Sistema 
Acad�mico 
15/07/2013 
11:40 
R00041 
-Grade 
Curricular 
(Curso) 


O 
Profissional: 
Os 
egressos 
do 
curso 
e 
Ci�ncia 
da 
Computa��o 
da 
UFBA 
devem 
estar 
situados 
no 
estado 
da 
arte 
da 
ci�ncia 
e 
da 
tecnologia 
da 
computa��o. 
Eles 
devem 
estar 
preparados 
para 
atuar 
no 
mercado 
de 
trabalho 
propondo 
solu��es 
adequadas 
que 
utilizem 
o 
computador 
bem 
como 
ter 
desenvolvimento 
tecnol�gico 
da 
�rea. 
Eles 
devem 
ter 
uma 
base 
cient�fica 
que 
os 
tornem 
aptos 
a 
se 
tornarem 
futuros 
pesquisadores, 
de 
maneira 
a 
contribuir 
com 
o 
desenvolvimento 
cient�fico 
da 
Computa��o. 
Mais 
especificamente, 
espera-se 
do 
egresso 
este 
curso: 
* 
uma 
forma��o 
b�sica 
dos 
aspectos 
te�ricos 
e 
pr�ticos 
das 
mat�rias 
b�sicas 
e 
tecnol�gicas 
da 
Ci�ncia 
da 
Computa��o. 
* 
uma 
forma��o 
especializada 
em 
uma 
ou 
mais 
�reas 
da 
Ci�ncia 
da 
Computa��o, 
oferecidas 
pelo 
curso; 
* 
uma 
maturidade 
matem�tica 
para 
entender 
e 
aplicar 
os 
modelos 
matem�ticos 
utilizados 
na 
representa��o 
de 
problemas 
da 
�rea; 
* 
uma 
forma��o 
complementar 
que 
o 
torne 
apto 
a 
atuar 
no 
mercado 
de 
trabalho 
em 
�reas 
multidisciplinares; 
* 
capacidade 
para 
atuar 
no 
mercado 
de 
trabalho 
nas 
diversas 
�reas 
tecnol�gicas 
da 
computa��o; 
* 
capacidade 
para 
ingressar 
em 
programas 
de 
p�s-gradua��o 
em 
Ci�ncia da Computa��o ou �reas afins. 


Aten��o: 
Os 
curr�culos 
dos 
cursos 
de 
gradua��o 
da 
Universidade 
Federal 
da 
Bahia 
est�o 
em 
processo 
de 
reformula��o 
curricular, 
com 
base 
nas 
Diretrizes 
Curriculares 
Nacionais. 
Desta 
forma, 
esta 
grade 
pode 
ainda 
n�o 
contemplar 
as 
mudan�as 
em 
andamento 
e 
em 
fase 
de 
implanta��o. 
Consulte 
o 
coordenador do curso para esclarecer poss�veis d�vidas. 


P�g. 
4 
de 
4 



