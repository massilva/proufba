UFBA 
-Universidade 
Federal 
da 
Bahia 
-Sistema 
Acad�mico 
03/09/2013 
14:28 
R00041 
-Grade 
Curricular 
(Curso) 
Curso: 
112140 
Curr�culo: 
1991-1 
Turno: 
Diurno 
Dura��o 
em 
anos: 
M�nima 
4 
M�dia 
5 
M�xima 
7 


Processamento de Dados 


�rea: 
Matem�tica, 
Ci�ncias 
F�sicas 
e 
Tecnologia 
Titula��o: 
Bacharel 
em 
Ci�ncia 
da 
Computa��o 


Habilita��o: 
Bacharelado 


Base 
Legal: 
AUTORIZA��O: RESOLU��O CONSUNI/UFBA N� 04 DE 22.01.1971. PARECER CFE N� 417/80, APROVADO EM 09.04.1980. 
RECONHECIMENTO: DECRETO N� 82027 DE 24.07.78. PARECER CFE N� 1910/78 


1�SEMESTRECr�dito/Semestre16Horas/Semestre30020Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MAT002 
MATEMATICA 
BASICA 
II 
90 
5 
CM 
MAT146 
INTRODUCAO 
A 
LOGICA 
DE 
PROGRAMACAO 
60 
3 
CM 


MAT150 
INTRODUCAO 
AOS 
SISTEMAS 
DE 
COMPUTACAO 
60 
3 
CM 
MAT195 
CALCULO 
DIFERENCIAL 
INTEGRAL 
I 
90 
5 
CM 


2�SEMESTRECr�dito/Semestre24Horas/Semestre49533Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
ELT453 
ELETIVA 
45-3 
45 
3 
EL 
FIS121 
FISICA 
GERAL 
E 
EXPERIMENTAL 
I-E 
90 
4 
CM 
MAT017 
ALGEBRA 
LINEAR 
I 
60 
3 
CM 
01 MAT002 
MAT042 
CALCULO 
II-A 
90 
4 
CM 
01 MAT002 MAT195 
MAT144 
ALGEBRA 
I 
A 
60 
3 
CM 
MAT147 
LINGUAGENS 
DE 
PROGRAMACAO 
I 
90 
4 
CM 
01 MAT146 MAT150 
OPT603 
OPTATIVA 
60-3 
60 
3 
OP 


3�SEMESTRECr�dito/Semestre24Horas/Semestre49533Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
ELT453 
ELETIVA 
45-3 
45 
3 
EL 
FIS122 
FISICA 
GERAL 
E 
EXPERIMENTAL 
II-E 
90 
4 
CM 
01 FIS121 MAT002 MAT195 
MAT018 
ALGEBRA 
LINEAR 
II 
60 
3 
CM 
01 MAT017 
MAT043 
CALCULO 
III-A 
90 
4 
CM 
01 MAT042 
MAT148 
LINGUAGENS 
DE 
PROGRAMACAO 
II 
90 
4 
CM 
01 MAT147 
MAT151 
ORGANIZACAO 
DE 
COMPUTADORES 
60 
3 
CM 
01 MAT146 MAT150 
MAT156 
TEORIA 
DOS 
GRAFOS 
60 
3 
CM 
01 MAT017 MAT144 


4�SEMESTRECr�dito/Semestre17Horas/Semestre34523Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MAT046 
ESTATISTICA 
III 
D 
90 
5 
CM 
01 MAT043 
MAT112 
ESTRUTURAS 
DE 
INFORMACAO 
90 
4 
CM 
01 MAT148 
MAT149 
LINGUAGENS 
DE 
MONTAGEM 
90 
4 
CM 
01 MAT151 
MAT174 
CALCULO 
NUMERICO 
I 
75 
4 
CM 
01 MAT017 MAT043 MAT147 


5�SEMESTRECr�dito/Semestre16Horas/Semestre30020Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MAT152 
ESTRUTURAS 
DE 
ARQUIVOS 
90 
4 
CM 
01 MAT112 
MAT155 
PROGRAMACAO 
MATEMATICA 
90 
5 
CM 
01 MAT018 MAT046 MAT147 
MAT158 
TEORIA 
DA 
COMPUTACAO 
60 
4 
CM 
01 MAT144 
OPT603 
OPTATIVA 
60-3 
60 
3 
OP 


6�SEMESTRECr�dito/Semestre18Horas/Semestre36024Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MAT154 
ARQUITET 
SISTEMAS 
OPERACIONAIS 
90 
4 
CM 
01 MAT112 MAT151 MAT155 
MAT157 
LINGUAG 
FORMAIS 
TEO 
COMPILACAO 
90 
5 
CM 
01 MAT158 
MAT161 
ENGENHARIA 
DE 
PROGRAMACAO 
60 
3 
CM 
01 MAT112 MAT158 
OPT603 
OPTATIVA 
60-3 
60 
3 
OP 
OPT603 
OPTATIVA 
60-3 
60 
3 
OP 


7�SEMESTRECr�dito/Semestre15Horas/Semestre30020Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MAT153 
ESTRUT 
TRADUTORES 
E 
MONTADORES 
60 
3 
CM 
01 MAT149 MAT157 
MAT160 
MODEL 
PROBABILIST 
PESQ 
OPERAC 
90 
4 
CO 
01 MAT155 
MAT162 
ANAL 
PROJ 
SIST 
DE 
INFORMACAO 
I 
90 
5 
CM 
01 MAT152 MAT154 
OPT603 
OPTATIVA 
60-3 
60 
3 
OP 


8�SEMESTRECr�dito/Semestre18Horas/Semestre34523Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
MAT159 
COMPUTADORES 
E 
SOCIEDADE 
45 
3 
CM 


P�g. 
1 
de 
2 



UFBA 
-Universidade 
Federal 
da 
Bahia 
-Sistema 
Acad�mico 
03/09/2013 
14:28 
R00041 
-Grade 
Curricular 
(Curso) 


8�SEMESTRECr�dito/Semestre18Horas/Semestre34523Horas/Semana
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 


MAT163 
ANALISE 
E 
PROJETO 
DE 
SISTEMAS 
DE 
INFORMA 
60 
3 
CM 
01 MAT162 
MAT164 
TELEPROCESSAMENTO 
90 
5 
CO 
01 MAT154 
MAT165 
BANCODE 
DADOS 
90 
4 
CO 
01 MAT152 MAT162 
OPT603 
OPTATIVA 
60-3 
60 
3 
OP 


OPTATIVAS
Disciplina 
C.H. 
CR 
Nat. 
Gr 
Pr� 
Requisito 
ADM001 
INTRODUCAO 
� 
ADMINISTRACAO 
60 
4 
OP 
ADM006 
ELEMEN 
ANALISE 
CUSTOS 
60 
4 
OP 
01 ECO004 
ADM100 
ADMINISTRACAO 
CONTABIL 
I 
60 
4 
OP 
01 ECO004 
ECO004 
CONTABILIDADE 
GERAL 
I 
60 
4 
OP 
ECO005 
CONTABILIDADE 
GERAL 
II 
60 
4 
OP 
01 ECO004 
ECO123 
CONTAB 
COMERCIAL 
I 
60 
4 
OP 
01 ECO004 
ECO127 
CONTAB 
INDUST 
I 
60 
4 
OP 
01 ECO005 
FCH162 
PSICOLOGIA 
DAS 
RELACOES 
HUMANAS 
60 
4 
OP 
FIS123 
FISICA 
GERAL 
E 
EXPERIMENTAL 
III-E 
90 
4 
OP 
01 FIS122 
FIS124 
FISICA 
GERAL 
E 
EXPERIMENTAL 
IV-E 
90 
4 
OP 
01 FIS123 
LET358 
INGLES 
INSTRUMENTAL 
III 
N-100 
45 
3 
OP 
LET359 
INGLES 
INSTRUMENTAL 
IV 
N-100 
45 
3 
OP 
MAT007 
CALCULO 
IV 
75 
4 
OP 
01 MAT042 
MAT105 
FUNCOES 
ANALITICAS 
I 
75 
4 
OP 
01 MAT042 


MAT166 
ADMINISTRACAO 
DE 
CENTROS 
DE 
PROCESSAME 
60 
3 
OP 
01 MAT112 
MAT167 
TOPICOS 
EM 
CIENCIA 
DA 
COMPUTACAO 
60 
3 
OP 
01 MAT158 
MAT168 
TOPICOS 
EM 
SISTEMAS 
DE 
INFORMACAO 
60 
3 
OP 
01 MAT154 
MAT169 
TOPICOS 
SISTEMAS 
DE 
COMPUTACAO 
60 
3 
OP 
01 MAT151 
MAT170 
INTRODUCAO 
A 
ANALISE 
NUMERICA 
60 
3 
OP 
01 MAT174 
MAT171 
LINGUAGENS 
DE 
PROGRAMACAO 
III 
60 
3 
OP 
01 MAT148 
MAT172 
INTRODUCAO 
AO 
PROJETO 
DE 
SISTEMAS 
DIGIT 
60 
3 
OP 
01 MAT151 
MAT208 
GEOMETRIA 
DIFERENCIAL 
90 
4 
OP 


INTEGRALIZA��O 
CURRICULAR 
Natureza 
Carga 
Hor�ria 
Credita��o 
Disciplina 
Nome 
M�xima 
M�nima 
M�xima 
M�nima 


CO 
Complementar 
Obrigatoria 
270 
270 
13 
13 
CM 
Curriculo 
Minimo 
2295 
2295 
115 
115 
EL 
Eletiva 
90 
90 
6 
6 
OP 
Optativa 
360 
360 
18 
18 
Total 
3015 
3015 
152 
152 


Disciplina 
Eletiva 
(EL) 
Objetiva 
complementar 
os 
cr�ditos 
necess�rios 
� 
intergraliza��o 
curricular, 
ampliando 
a 
forma��o 
cultural 
do 
estudante, 
possibilitando 
inclusive, 
a 
sua 
integra��o com outras �reas do conhecimento, independente da sua op��o de curso. De livre escolha do estudante dentre as oferecidas pela UFBA. 


Observa��o: 
Exclus�o das disciplinas de Pratica Desportiva do elenco das obrigat�rias dos curr�culos da UFBa pelo Parecer n� 375/97 de 11/06/97 da C�mara de 
Ensino de Gradua��o; poder�o ser consideradas eletivas para integraliza��o curricular. 
Resolu��o n�001/94 da C�mara de Ensino de Gradua��o extingue o ensino da disciplina Estudo de Problemas Brasileiros e autoriza os Colegiados de 
Cursos utilizarem os cr�ditos cursados da disciplina para integraliza��o curricular. 


O 
Profissional: 
O 
Bacharel 
em 
Processamento 
de 
Dados 
� 
o 
profissional 
respons�vel 
pela 
an�lise, 
defini��o 
e 
constru��o 
de 
sistemas 
de 
programa��o 
para 
computadores. 
Estes 
sistemas 
podem 
estar 
voltados 
para 
solu��o 
de 
problemas 
dentro 
de 
organiza��es 
p�blicas 
e 
privadas, 
como 
tamb�m 
de 
problemas 
envolvidos 
no 
processo 
de 
desenvolvimento 
de 
software 
do 
tipo 
de 
compiladores 
e 
sistemas 
operacionais. 
Al�m 
disto 
este 
profissional 
ser� 
capaz 
de 
realizar 
adapta��es 
em 
sistemas 
de 
programa��o 
existentes, 
visando 
adequar 
os 
mesmo 
�s 
necessidades 
locais 
espec�ficas. 
Outra 
caracter�stica 
marcante 
no 
profissional 
que 
se 
visa 
formar 
se 
prende 
ao 
fato 
do 
curso 
procurar 
torn�-lo 
apto 
a 
empregar 
as 
mais 
avan�adas 
t�cnicas 
de 
programa��o, 
metodologias 
de 
projetos 
e 
metodologias 
de 
constru��o 
de 
modelos 
matem�ticos 
tanto 
para 
a 
solu��o 
de 
problemas 
na 
�rea 
cient�fica 
como na comercial e industrial. 


Aten��o: 
Os 
curr�culos 
dos 
cursos 
de 
gradua��o 
da 
Universidade 
Federal 
da 
Bahia 
est�o 
em 
processo 
de 
reformula��o 
curricular, 
com 
base 
nas 
Diretrizes 
Curriculares 
Nacionais. 
Desta 
forma, 
esta 
grade 
pode 
ainda 
n�o 
contemplar 
as 
mudan�as 
em 
andamento 
e 
em 
fase 
de 
implanta��o. 
Consulte 
o 
coordenador do curso para esclarecer poss�veis d�vidas. 


P�g. 
2 
de 
2 



