<?php

	function create_table(){

		$sql = "
		CREATE TABLE IF NOT EXISTS curso(
			codigo				int PRIMARY KEY,
			nome				varchar(45) NOT NULL
		);";
		$query00 = pg_query($sql);

		if($query00){
			echo "Tabela curso foi criada<br>";
		}else{
			echo "Tabela curso nao foi criada<br>";
		}
		
		$sql = "
		CREATE TABLE IF NOT EXISTS curriculo(
			curso_codigo			int REFERENCES curso(codigo),
			periodo_criado			int,			
			PRIMARY KEY(curso_codigo,periodo_criado)
		);";
		$query00 = pg_query($sql);

		if($query00){
			echo "Tabela curriculo foi criada<br>";
		}else{
			echo "Tabela curriculo nao foi criada<br>";
		}
		
		$sql = "
		CREATE TABLE IF NOT EXISTS disciplina(
			codigo					varchar(45) PRIMARY KEY,
			nome					varchar(45) NOT NULL,
			carga_horaria			int NOT NULL
		);";

		$query00 = pg_query($sql);

		if($query00){
			echo "Tabela disciplina foi criada<br>";
		}else{
			echo "Tabela disciplina nao foi criada<br>";
		}
		
//		$sql = "
//		CREATE TABLE IF NOT EXISTS curriculo_contem_requisito(
//			requisito_curso_codigo 		INT, 
//			requisito_periodo_criado 	INT, 
//			disciplina_codigo 			VARCHAR(45) references disciplina(codigo),
//			requisito_codigo 			VARCHAR(45) references disciplina(codigo)
//		);";
//		$query00 = pg_query($sql);
//
//		if($query00){
//			echo "Tabela curriculo_contem_requisito foi criada<br>";
//		}else{
//			echo "Tabela curriculo_contem_requisito nao foi criada<br>";
//		}		

		$sql = "
		CREATE TABLE IF NOT EXISTS disciplina_contem_requisito(
			requisito_curso_codigo 		INT, 
			requisito_periodo_criado 	INT, 
			disciplina_codigo 			VARCHAR(45) references disciplina(codigo),
			requisito_codigo 			VARCHAR(45) references disciplina(codigo)
		);";
		$query00 = pg_query($sql);

		if($query00){
			echo "Tabela curriculo_contem_requisito foi criada<br>";
		}else{
			echo "Tabela curriculo_contem_requisito nao foi criada<br>";
		}		
                
		$sql = "
		CREATE TABLE IF NOT EXISTS aluno(
			matricula					int PRIMARY KEY,
			curriculo_curso_codigo		int,
			curriculo_periodo_criado	int,
			coeficiente_rendimento		float,
			ano_equivalencia			int,
			periodo_saida				int,
			motivo_saida				varchar(45),
			data_nascimento				date NOT NULL,
			naturalidade				varchar(45) NOT NULL,
			nacionalidade				varchar(45) NOT NULL,
			FOREIGN KEY (curriculo_curso_codigo,curriculo_periodo_criado) REFERENCES curriculo(curso_codigo,periodo_criado)
		);";
		
		$query00 = pg_query($sql);
		if($query00){
			echo "Tabela aluno foi criada<br>";
		}else{
			echo "Tabela aluno nao foi criada<br>";
		}		

		$sql = "
		CREATE TABLE IF NOT EXISTS periodo(
			aluno_matricula			int REFERENCES aluno(matricula),
			semestre				int,
			media_nota				float,
			situacao_periodo		varchar(45) NOT NULL,
			detalhes				varchar(45),
			PRIMARY KEY(aluno_matricula,semestre)
		);";
		
		$query00 = pg_query($sql);

		if($query00){
			echo "Tabela periodo foi criada<br>";
		}else{
			echo "Tabela periodo nao foi criada<br>";
		}		

		$sql = "
		CREATE TABLE IF NOT EXISTS periodo_contem_disciplina(
			periodo_semestre			int,
			periodo_aluno_matricula		int,
			e_itensivo					boolean,	
			disciplina_codigo			varchar(45),
			nota						float,
			resultado					varchar(45),
			PRIMARY KEY(periodo_semestre,periodo_aluno_matricula,disciplina_codigo,e_itensivo),
			FOREIGN KEY(periodo_semestre,periodo_aluno_matricula) REFERENCES periodo(semestre,aluno_matricula),
			FOREIGN KEY(disciplina_codigo) REFERENCES disciplina(codigo)
		);";
		
		$query00 = pg_query($sql);

		if($query00){
			echo "Tabela periodo_contem_disciplina foi criada<br>";
		}else{
			echo "Tabela periodo_contem_disciplina nao foi criada<br>";
		}		

		$sql = "
		CREATE TABLE IF NOT EXISTS curriculo_contem_disciplina(
			curriculo_curso_codigo		int,
			curriculo_periodo_criado	int,
			disciplina_codigo		varchar(45),
			natureza			varchar(45) NOT NULL,
			semestre_recomendado		int NOT NULL,
			PRIMARY KEY(curriculo_curso_codigo,curriculo_periodo_criado,disciplina_codigo),
			FOREIGN KEY(curriculo_curso_codigo,curriculo_periodo_criado) REFERENCES curriculo(curso_codigo,periodo_criado),
			FOREIGN KEY(disciplina_codigo) REFERENCES disciplina(codigo)
		);";

		$query00 = pg_query($sql);

		if($query00){
			echo "Tabela curriculo_contem_disciplina foi criada<br>";
		}else{
			echo "Tabela curriculo_contem_disciplina nao foi criada<br>";
		}

	}
	
?>
