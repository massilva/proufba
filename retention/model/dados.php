<?php

class Dados{

	//Pegando o arquivo
	private $file; 
	private $string;

	//$todas_disciplinas é a array de todas as disciplinas que existem na faculdade;
	private $todas_disciplinas;
	
	//Tabela disciplina
	private $codigo;		
	private $nome;
	private $CH;
	
	//Tabela curriculo_contem_disciplina
	private $curriculo_curso_codigo;
	private $curriculo_periodo_criado;
	private $disciplina_codigo;
	private $natureza;
	private $semestre_recomendado;
	
	private $semestre;
	private $ocorrencia00;
	private $ocorrencia01;	
	private $opt_natureza;
	
	//Tabela disciplina_contem_requisito		
	private $requisito_curso_codigo;	
	private $requisito_periodo_criado;
	private $requisito_codigo;	
	private $periodo;
	
	function __construct()
	{

		//Pegando o arquivo
		$file =  getcwd()."/grade_si.txt"; 
		
		$string = NULL;

		//$todas_disciplinas é a array de todas as disciplinas que existem na faculdade;
		$todas_disciplinas = array("MATA02","MATA37","MATA39","MATA42","MATA68","ADME99","MATC73","MATC90","MATC92","MATD04","ADM001","MATA07","MATA55","MATA58",
		"MATC94","LETA09","MAT236","MATA59","MATA62","MATC82","ADM211","MATA56","MATA60","MATA63","MATC84","ADMF01","MAT220","MATA76","MATB09","MATC89",
		"MATA64","MATB02","MATB19","MATC72","MATC99"/*,"OPT068","OPT0"*/,"MATC97","MATC98","ADM241","ECO001","FCC024","LETA15","LETC84","LETE46","MATA57",
		"MATA88","MATB16","MATC88","MATC91","MATC93","MATC95","MATC96","MATD01","MATD02","MATD55","MATA01","MATA38","FCHC45","MATA40","MATA95","MATA97",
		"FISA75","MATA47","MATA50","MATA96","FISA76","MATA48","MATA51","MATA52","MATA49","MATA53","MATA54","MATA61","MATA65","MATA66","ADM171","ADM243",
		"EDC001","ENG229","ENG336","ENG656","ENG648","FCC001","FCH162","LET358","LET359","MAT174","MATA04","MATA05","MATA27","MATA41","MATA69","MATA71",
		"MATA72","MATA73","MATA74","MATA77","MATA79","MATA80","MATA81","MATA82","MATA83","MATA85","MATA86","MATA87","MATA89","MATA90","MATAB01",
		"MATAB03","MATAB05","MATAB06","MATAB09","MATAB10","MATAB11","MATAB12","MATAB13","MATAB14","MATAB15","MATAB17","MATAB18","MATAB20","MATAB21",
		"MATAB22","MATAB23","MATAB24","MATAB25","MATAB26","MATAB27","MATAB28","MATAB29","MATAB");
		
		//Tabela disciplina
		$codigo = NULL;		
		$nome = NULL;
		$CH = NULL;
		
		//Tabela curriculo_contem_disciplina
		$curriculo_curso_codigo = NULL;
		$curriculo_periodo_criado = NULL;
		$disciplina_codigo = NULL;
		$natureza = NULL;
		$semestre_recomendado = NULL;
		
		$semestre = 0;
		$ocorrencia00 = NULL;
		$ocorrencia01 = NULL;	
		$opt_natureza = array("OB","OP");	
		
		//Tabela disciplina_contem_requisito		
		$requisito_curso_codigo = NULL;	
		$requisito_periodo_criado = NULL;
		$requisito_codigo = NULL;	
		$periodo = NULL;
		
		$this->string = txttostring($file);
		//$string = file_get_contents($file);

	}

	public function getString()
	{
		return $this->string;
	}

	public function __set($name, $value) {
		$methodName = 'set'.ucfirst($name);

      	if (method_exists($this, $methodName))
            $this->$methodName($value);
    	else
            $this->$name = $value;
    }
 
    public function __get($name) {
      $methodName = 'get'.ucfirst($name);
      if (method_exists($this, $methodName))
            return $this->$methodName($value);
      else
            return $this->$name;
    }  

}	

?>