<?php 
	/*
		Classe que persiste os dados de curriculo_contem_requisito e curriculo_contem_disciplina
	*/
	class Curriculo
	{

		private $curso_codigo;
		private $periodo_criado;
		private $disciplinas;
		
		function __construct($curso_codigo = "", $periodo_criado = "", $disciplinas = array())
		{
			$this->curso_codigo = $curso_codigo;
			$this->periodo_criado = $periodo_criado;
			$this->disciplinas = $disciplinas;
		}

		public function setCursoCodigo($curso_codigo){
			$this->curso_codigo = $curso_codigo;
		}

		public function getCursoCodigo(){
			return $this->curso_codigo;
		}
		
		public function setPeriodoCriado($periodo_criado){
			$this->periodo_criado = $periodo_criado;
		}

		public function getPeriodoCriado(){
			return $this->periodo_criado;
		}
		
		public function setDisciplinas($disciplinas){
			$this->disciplinas = $disciplinas;
		}

		public function getDisciplinas(){
			return $this->disciplinas;
		}

		// Adiciona a disciplina $disciplina na posição $pos
		public function addDisciplina($disciplina,$pos = ""){
			if($pos == "")
			{
				$pos = $disciplina->codigo;
			}
			$this->disciplinas[$pos] = $disciplina;
		}

		public function getDisciplina($disciplina_codigo){
			return $this->disciplinas[$disciplina_codigo];
		}

		public function inDisciplinas($disciplina_codigo){
			return in_array($disciplina_codigo, $this->disciplinas);
		}

		public function __set($name, $value) {
			$methodName = 'set'.ucfirst($name);
			if (method_exists($this, $methodName))
	            $this->$methodName($value);
	    	else
	            $this->$name = $value;
	    }
	 
	    public function __get($name) {
			$methodName = 'get'.ucfirst($name);
			if (method_exists($this, $methodName))
			    return $this->$methodName();
			else
    			return $this->$name;
	    } 

	}

?>