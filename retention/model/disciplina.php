<?php
	
	class Disciplina{

		/* Atributos da classe*/
		private $codigo = "";
		private $nome = "";
		private $carga_horaria;
		private $natureza;
		private $semestre_recomendado;
		private $requisitos;

		function __construct($codigo = "",$nome = "", $carga_horaria = 0, $natureza = "OP", $semestre_recomendado = 0, $requisitos = array())
		{
			$this->codigo = $codigo;
			$this->nome = $nome;
			$this->carga_horaria = $carga_horaria;
			$this->natureza = $natureza;
			$this->semestre_recomendado = $semestre_recomendado;
			$this->requisitos = $requisitos;
		}

		public function setCodigo($codigo){
			$this->codigo = $codigo;
		}

		public function getCodigo(){
			return $this->codigo;
		}

		public function setNome($nome){
			$this->nome = $nome;
		}

		public function getNome(){
			return $this->nome;
		}
		
		public function setCargaHoraria($carga_horaria){
			$this->carga_horaria = $carga_horaria;
		}

		public function getCargaHoraria(){
			return $this->carga_horaria;
		}
		
		public function setNatureza($natureza){
			$this->natureza = $natureza;
		}

		public function getNatureza(){
			return $this->natureza;
		}
		
		public function setSemestreRecomendado($semestre_recomendado){
			$this->semestre_recomendado = $semestre_recomendado;
		}

		public function getSemestreRecomendado(){
			return $this->semestre_recomendado;
		}
		
		public function setRequisitos($requisitos){
			$this->requisitos = $requisitos;
		}

		public function getRequisitos(){
			return $this->requisitos;
		}

		public function addRequisito($requisito){
			$this->requisitos[$requisito->codigo] = $requisito;
		}

		public function getRequisito($disciplina_codigo){
			return $this->requisitos[$disciplina_codigo];
		}

		public function __set($name, $value) {
			$methodName = 'set'.ucfirst($name);
			if (method_exists($this, $methodName))
	            $this->$methodName($value);
	    	else
	            $this->$name = $value;
	    }
	 
	    public function __get($name) {
			$methodName = 'get'.ucfirst($name);
			if (method_exists($this, $methodName))
			    return $this->$methodName();
			else
    			return $this->$name;
	    } 

	    public function toString()
	    {

	    	$string = "<table width='100%' border='0'>
		    				<tr>
		    					<td>Codigo: </td><td width='75%'>".$this->codigo."</td>
		    				</tr>
		    				<tr>
		    					<td>Nome: </td><td>".$this->nome."</td>
	    					</tr>
	    					<tr>
	    						<td>C.H.: </td><td>".$this->carga_horaria."</td>
	    					</tr>
	    						<td>Natureza: </td><td>".$this->natureza."</td>
	    					</tr>
	    						<td>Semestre Recomendado: </td><td>".$this->semestre_recomendado."</td>
	    					</tr>
	    					<tr bgcolor='#ff9900'> 
	    						<td>Requisitos: </td><td>";

    		foreach ($this->requisitos as $requisito)
    		{
    			$string .= $requisito->codigo." ";
			}			

			$string .= "</tr></table>";

			return $string;
	    }
	}

?>