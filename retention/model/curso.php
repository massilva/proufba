<?php 
	
	class Curso
	{
		
		private $codigo;
		private $nome;

		function __construct($codigo = "", $nome = "")
		{
			$this->codigo = $codigo;
			$this->nome = $nome;
		}
	
	    /**
	     * Gets the value of codigo.
	     *
	     * @return mixed
	     */
	    public function getCodigo()
	    {
	        return $this->codigo;
	    }

	    /**
	     * Sets the value of codigo.
	     *
	     * @param mixed $codigo the codigo
	     *
	     * @return self
	     */
	    public function setCodigo($codigo)
	    {
	        $this->codigo = $codigo;

	        return $this;
	    }

	    /**
	     * Gets the value of nome.
	     *
	     * @return mixed
	     */
	    public function getNome()
	    {
	        return $this->nome;
	    }

	    /**
	     * Sets the value of nome.
	     *
	     * @param mixed $nome the nome
	     *
	     * @return self
	     */
	    public function setNome($nome)
	    {
	        $this->nome = $nome;

	        return $this;
	    }

		public function __set($name, $value) {
			$methodName = 'set'.ucfirst($name);
			if (method_exists($this, $methodName))
	            $this->$methodName($value);
	    	else
	            $this->$name = $value;
	    }
	 
	    public function __get($name) {
			$methodName = 'get'.ucfirst($name);
			if (method_exists($this, $methodName))
			    return $this->$methodName();
			else
    			return $this->$name;
	    } 

	}

?>