<?php 
	
	class Requisito
	{
		
		private $disciplina_codigo;
		private $requisito_codigo;

		function __construct($disciplina_codigo = "",$requisito_codigo = ""){
			$this->disciplina_codigo = $disciplina_codigo;
			$this->requisito_codigo = $requisito_codigo;
		}

		public function setDisciplinaCodigo($disciplina_codigo)
		{
			$this->disciplina_codigo = $disciplina_codigo;
		}

		public function getDisciplinaCodigo()
		{
			return $this->disciplina_codigo;
		}

		public function setRequisitoCodigo($requisito_codigo)
		{
			$this->requisito_codigo = $requisito_codigo;
		}

		public function getRequisitoCodigo()
		{
			return $this->requisito_codigo;
		}

		public function __set($name, $value) {
			$methodName = 'set'.ucfirst($name);
			if (method_exists($this, $methodName))
	            $this->$methodName($value);
	    	else
	            $this->$name = $value;
	    }
	 
	    public function __get($name) {
			$methodName = 'get'.ucfirst($name);
			if (method_exists($this, $methodName))
			    return $this->$methodName();
			else
    			return $this->$name;
	    } 

	}

?>