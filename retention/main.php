<?php 

require_once "connect_pg.php";
require_once "constantes.php";
require_once "txttostring.php";	
require_once DAO_PATH."create_table.php";
require_once DAO_PATH."insert_dados.php";

/*
* @param a pasta onde estão os txts das grades
* @retorn um array com a lista de todos os arquivos .txt dentro da pasta $path
*/
function listaArquivos($path = "arquivos/")
{

	$listaArquivos = array();
   	$diretorio = dir($path);
 	
   	while($arquivo = $diretorio->read()){
   		if($arquivo != ".." && $arquivo != ".")
   		{
   			$extensao = substr($arquivo,-4);//pegando a extensao do arquivo
   			//verificando se a extensao é .txt
   			if($extensao == ".txt"){
   				$listaArquivos[] = "/".$path.$arquivo;
   			}
   		}
   	}

   	$diretorio->close();
   	return $listaArquivos;

}

/*
* @param a pasta onde estão os txts das grades
* @retorn um array com a lista de todos os arquivos .txt dentro da pasta $path
*/
function listaPDF($path = "pdf/")
{

	$listaArquivos = array();
   	$diretorio = dir($path);
 	
   	while($arquivo = $diretorio->read()){
   		if($arquivo != ".." && $arquivo != ".")
   		{
   			$extensao = substr($arquivo,-4);//pegando a extensao do arquivo
   			//verificando se a extensao é .txt
   			if($extensao == ".pdf"){
   				$listaArquivos[] = "/".$path.$arquivo;
   			}
   		}
   	}

   	$diretorio->close();
   	return $listaArquivos;

}

/*
* @param um string 
* @return a string passada como parametro sem acentos.
*/
function sem_acentos($string) 
{ 
	
	$array_acentos = array( "á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç" , "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç" ); 

	$array_sem_acentos = array( "a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c" , "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C" ); 

	$txt = str_replace($array_acentos, $array_sem_acentos, $string);

	return $txt;

} 

/*
* @param uma string
* @return boolean informando se a $codigo é um codigo de disciplina válido(true) ou não(false).
*/
function ehCodigoValido($codigo)
{
	$er = "/^(([a-zA-Z]){3})([0-9]{3}||(([a-zA-Z][0-9]){1}[0-9]{1})||([a-zA-Z]{2}[0-9]{1}))$/";
	return ((preg_match($er, $codigo)) && strlen($codigo) == 6);
}

/*
* @param caminho de onde está o arquivo.
* @return mostra, e salva no BD, o conteúdo do arquivo caso seja uma grade.
*/
function salvaGrade($file)
{

	//Estanciando um novo curriculo - Inicialmente vázio.
	$curriculo = new Curriculo();

	//passa o txt para array $string
	$string = txttostring($file);	

	//inicio do loop da var string
	$fi = count($string);		

	//flags para saber quando está copiando o nome da disciplina ou se é requisito
	$is_nome_disciplina = false;
	$is_requisito = false;
	$is_curso_nome = false;

	$curso_nome = $nome_disciplina = "";
	$disciplina_atual = new Disciplina();

	$semestre_recomendado  = 0;
	$fim_grade = false;

	//Pegando informações sobre as disciplinas e o curriculo.
	for($i = 0; $i < $fi && !$fim_grade; $i++)
	{ 
		
		if($string[$i] == "Disciplina")
		{
			$semestre_recomendado++;
		}

		if($string[$i] == utf8_decode("�rea:"))
		{
			$is_curso_nome = false;
		}

		if($is_curso_nome == true)
		{
			$curso_nome .= $string[$i]." ";
		}

		if($curso_nome == "" && (strtolower($string[$i-1]) == utf8_decode("m�xima")))
		{
			$is_curso_nome = true;
		}
                
        if(!strcasecmp($string[$i+1],"semestre")){
            $is_nome_disciplina = false;
        }
                                
//		if(!(strstr($string[$i],"Disciplina")) && 
//		   !(strstr($string[$i],"C.H.")) && 
//		   !(strstr($string[$i],"CR")) && 
//		   !(strstr($string[$i],"Nat.")) && 
//		   !(strstr($string[$i],"Gr")) && 
//		   !(strstr($string[$i],"Pré")) && 
//		   !(strstr($string[$i],"Requisito")))
//		{
            if(strcmp($string[$i+3],"OP"))
            {
                
                if($string[$i+1]=="P�g.")
                {
                    $is_nome_disciplina = false;
                    $nome_disciplina = "";
                }

                if(!strcasecmp($string[$i],"Disciplina"))
                {
                   $i+=7;
                }
                
			if($is_nome_disciplina && !$is_requisito)
			{
				
				$name = sem_acentos(utf8_encode($string[$i]));

				if($name != "INTEGRALIZACAO")
				{
					$nome_disciplina .= " ".$string[$i];
				}
				else
				{
					$is_nome_disciplina = $is_requisito = false;
					$fim_grade = true;
				}

			}

			/*
				Se $string[$i+3] for a natureza
				$string[$i+1] = carga horária 
				$string[$i+2] = CR
			*/
			if( $is_nome_disciplina && 
				(!strcmp($string[$i+3],"CO") || 
				 !strcmp($string[$i+3],"ES") || 
				 !strcmp($string[$i+3],"CM") || 
				 !strcmp($string[$i+3],"EL") || 
				 !strcmp($string[$i+3],"NU") || 
				 !strcmp($string[$i+3],"OB") || 
				 !strcmp($string[$i+3],"AC")
				) 
			  )
			{

				$nome = sem_acentos(utf8_encode($nome_disciplina));
				$disciplina_atual->nome = $nome;
				$disciplina_atual->natureza = $string[$i+3];
				$ch = $string[$i+1];
                                
				$aux_nome = " ";
                $aux_ch = "";
				if(!is_numeric($ch)){
	                $aux = 0;
	                while(($aux < strlen($ch)) && (!is_numeric($ch[$aux]))){
	                    $aux_nome .= $ch[$aux];
	                    $aux++;
	                }
	                while(($aux < strlen($ch)) && (is_numeric($ch[$aux]))){
	                    $aux_ch .= $ch[$aux];
	                    $aux++;
	                }
	                $ch = $aux_ch;
				}
                                
                                $disciplina_atual->nome .= $aux_nome;
				$disciplina_atual->cargaHoraria = $ch;
				
				if($string[$i+3] != "OP")
					$disciplina_atual->semestreRecomendado = $semestre_recomendado;
				else
					$disciplina_atual->semestreRecomendado = 0;

				$is_nome_disciplina = false; //chegou no fim do nome da disciplina
				$nome_disciplina = "";

				if($disciplina_atual->nome != "")
				{

					/*
					echo ":: ";
					var_dump($disciplina_atual);
					echo "<br/><br/>";
					*/
					//Adicionando a disciplina no curriculo com aquele código
					$d = $curriculo->getDisciplina($disciplina_atual->codigo);

					if($d->nome == "")
					{
						$curriculo->addDisciplina($disciplina_atual);
					}

					//var_dump($todas_disciplinas);
					//echo "<br/><br/>";
					//$todas_disciplinas[count($todas_disciplinas)] = $disciplina_atual;
				}
				$i += 3;

			}

			//Caso apareça um 01 a prox. posicao é o prerequisito
			if($string[$i] == "01")
			{
				$is_requisito = true;
			}

			/* Caso seja um código de uma matéria e não já esteja salva, guarda.*/
			if(!$is_requisito && ehCodigoValido($string[$i]) && !(in_array($string[$i],$curriculo->disciplinas)))
			{
                $disciplina_atual = new Disciplina($string[$i]);

                $d = $curriculo->getDisciplina($disciplina_atual->codigo);
                if($d->nome == "")
                {
                        $curriculo->addDisciplina($disciplina_atual);
                }

                $is_nome_disciplina = true;
			}
                        
			//Se for requisito
			if($string[$i] != "01" && $is_requisito)
			{

				$requisito = "";
				
				if(ehCodigoValido($string[$i]))
				{
					
					//echo $disciplina_atual->codigo." tem requisito(".$i."): ".$string[$i]." prox: ".$string[$i+1];

					$d = $curriculo->getDisciplina($string[$i]);

					//Se não estiver na lista de disciplinas, adiciona este na lista de disciplinas do curriculo
					if(empty($d))
					{
						$d = new Disciplina($string[$i]);
						$curriculo->addDisciplina($d);
					}

					$disciplina_atual->addRequisito($d);
					
					//echo "<br/>";
					//var_dump($disciplina_atual->requisitos);
					//echo "<br/>";
				}


				//Se for igual a Disciplina é mais um fim de semestre
				/*
				if($string[$i+2] == "Disciplina")
				{
					echo " FIM de mais um semestre ".$string[$i];
					echo "#".ehCodigoValido($string[$i+1])."#".ehCodigoValido($string[$i]);
				}
				*/
				if($string[$i+2] != "Disciplina")
				{
                                        
                                    	//Verificando se o proximo é um prerequisito
					// Se $string[$i+2] não for válido só há um requisito.
					if(!ehCodigoValido($string[$i+2]) || !ehCodigoValido($string[$i+1]))
					{
						$is_requisito = false;
					}
                                    
					/*
					Mostrar qual é o requisito e se o prox elemento do prox é um codigo válido 	para poder saber se a disciplina tem somente um requisito, no caso se $string[$i+2] for codigo válido então $string[$i] e $string[$i+1] são prequisitos.
					*/
					//echo " Requisito(".$i.") ".$string[$i];
					//echo "#".ehCodigoValido($string[$i+2])."#".ehCodigoValido($string[$i]);
				}
				//echo "<br/><br/>";

			}

                }
                else{
                    $is_nome_disciplina = false;
                    $nome_disciplina = "";
                }
//		}	
                    
		//Extraindo $curriculo_curso_codigo, $curriculo_periodo_criado e $requisito_curso_dodigo
		if("Turno:" == $string[$i])
		{

			/* 
				Setando o Codigo do curso 
				O mesmo que:
					$curriculo->setCursoCodigo($string[$i-3]);
			*/
			$curriculo->cursoCodigo = $string[$i-3];
			$nova_string = explode("-",$string[$i-1]);		
			/* 
				Setando o Período Criado 
				O mesmo que:
					$curriculo->setPeriodoCriado("$nova_string[0]$nova_string[1]");
			*/
			$curriculo->periodoCriado = "$nova_string[0]$nova_string[1]";

		}	
		
	}

	$curso_nome = sem_acentos(utf8_encode($curso_nome));

	echo "<hr>";
	echo "<p>";
	echo "Curso: ".$curriculo->cursoCodigo." - ".$curso_nome; 
	echo "<br/>";
	echo utf8_decode("Currículo: ").$curriculo->periodoCriado;
	echo "</p>";
	
	//Tabela disciplina
	$codigo = array();		
	$nome = array();
	$CH = array();
	
	//Tabela curriculo_contem_disciplina
	$disciplina_codigo = array();
	$natureza = array();
	$semestre_recomendado = array();
	
	//Tabela disciplina_contem_requisito		
	$disciplina_codigo_r = array();
	$requisito_codigo = array();
	
	//Preparando arrays para colocar no banco. Imprimindo tabelas.
	foreach ($curriculo->disciplinas as $disciplina)
	{
		
		$codigo[] = $disciplina->codigo;
		$nome[] = $disciplina->nome;
		$CH[] = $disciplina->cargaHoraria;
					
		$disciplina_codigo[] = $disciplina->codigo;
		$natureza[] = $disciplina->natureza;
		$semestre_recomendado[] = $disciplina->semestreRecomendado;
		
		if($disciplina->requisitos != NULL)
		{
			foreach($disciplina->requisitos as $requisito){
				$disciplina_codigo_r[] = $disciplina->codigo;
				$requisito_codigo[] = $requisito->codigo;
			}
			
		}
		echo $disciplina->toString(); //Se quiser que não apareça a tabela comente está linha
		
	}
/*
	echo "<p>";
	insert_curso($curriculo->cursoCodigo, $curso_nome);
	echo "</p>";
	echo "<p>";
	insert_curriculo($curriculo->cursoCodigo, $curriculo->periodoCriado);
	echo "</p>";
	echo "<p>";
	insert_disciplina($codigo, $nome, $CH);
	echo "</p>";
	echo "<p>";
	insert_curriculo_contem_disciplina($curriculo->cursoCodigo, $curriculo->periodoCriado, $codigo, $natureza, $semestre_recomendado);
	echo "</p>";
	echo "<p>";
	insert_disciplina_contem_requisito($curriculo->cursoCodigo, $curriculo->periodoCriado, $disciplina_codigo_r, $requisito_codigo);
	echo "</p>";
*/
}

// Função que chamar o script em python para minerar o historico
function callScriptPython(){
	shell_exec("python ".PATH_ATUAL."/../pdfminer/miner.py");
	passthru("python ".PATH_ATUAL."/../pdfminer/miner.py");
}

/*FUNÇÃO FPDF2Text*/
function FPDF2Text($filename){
	$a = new PDF2Text();
	$a->setFilename($filename); 
	$a->decodePDF();
	$out = $a->output();
    return $out;
}
    
/*
* Função principal
*/
function main()
{
	
	require_once MODEL_PATH."disciplina.php";
	require_once MODEL_PATH."curriculo.php";
	require_once MODEL_PATH."pdf2text.php";

	/*
	 	Pegando a lista de arquivos .txt do diretorio /arquivos
		síntaxe: listaArquivos($path = '/arquivos');
	*/
	$listaArquivos = listaArquivos();

	foreach ($listaArquivos as $grade) 
	{
		$file =  PATH_ATUAL.$grade;
	    salvaGrade($file);
	    echo "<p><hr></p>";
	}

}

//criando as tabelas caso não existam
//create_table();
main();
//callScriptPython();

?>