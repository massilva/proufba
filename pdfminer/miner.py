#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import re

# Important: CH can not be trusted from the transcript, there are a lot of -- for courses that should be assigned. A separated table with the information filled should be used. 
def pdf_to_csv(filename):
    
    from cStringIO import StringIO  
    from pdfminer.converter import LTChar, LTTextBox, TextConverter    #<-- changed
    from pdfminer.layout import LAParams
    from pdfminer.pdfparser import PDFDocument, PDFParser
    from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter

    class CsvConverter(TextConverter):
        def __init__(self, *args, **kwargs):
            TextConverter.__init__(self, *args, **kwargs)

        def end_page(self, i):
            from collections import defaultdict
            lines = defaultdict(lambda : {})
#            print dir(self.cur_item)
 #           sys.exit(0)
            for child in self.cur_item._objs:
#                if isinstance(child, LTTextBox):
#                    print child
                if isinstance(child, LTChar):               #<-- changed
                    (_,_,x,y) = child.bbox                   
                    line = lines[int(-y)]
                    line[x] = child._text.encode(self.codec)

            for y in sorted(lines.keys()):
                line = lines[y]
                self.outfp.write("".join(line[x] for x in sorted(line.keys())))
                self.outfp.write("\n")

    # ... the following part of the code is a remix of the 
    # convert() function in the pdfminer/tools/pdf2text module
    rsrc = PDFResourceManager()
    outfp = StringIO()
    device = CsvConverter(rsrc, outfp, codec="utf-8", laparams=LAParams())  #<-- changed
        # becuase my test documents are utf-8 (note: utf-8 is the default codec)

    doc = PDFDocument()
    fp = open(filename, 'rb')
    parser = PDFParser(fp)       
    parser.set_document(doc)     
    doc.set_parser(parser)       
    doc.initialize('')

    interpreter = PDFPageInterpreter(rsrc, device)

    for i, page in enumerate(doc.get_pages()):
        outfp.write("START_PAGE %d\n" % i)
        if page is not None:
            interpreter.process_page(page)
        outfp.write("END_PAGE %d\n" % i)

    device.close()
    fp.close()

    return outfp.getvalue()
    
class Aluno:
    matricula = ''
    data_nascimento = ''
    naturalidade = ''
    nacionalidade = ''
    courses = None
    periodo_saida = ''
    periodo_ingresso = ''
    ano_equivalencia = ''
    coeficiente_rendimento = ''
    curriculo = ''
    
#    courses = None #key are semesters and values contain a list of lists (each element of the upper level list contains the information of a given course ; name, code, ch, grade, etc)
    def __init__(self,matricula):
        self.matricula = matricula
        self.courses = {}
    def set_data_nascimento(self,data_nascimento):
        self.data_nascimento = data_nascimento
    def set_naturalidade(self,naturalidade):
        self.naturalidade = naturalidade.strip()
    def set_nacionalidade(self,nacionalidade):
        self.nacionalidade = nacionalidade.strip()
    def course_init(self,period):
        self.courses[period] = []
    def add_course(self,course_semester,course_information):
        self.courses[course_semester].append(course_information)
    def set_periodo_saida(self,periodo_saida_and_reason): #Receives a tuple where [0] is periodo_saida and [1] is reason
        self.periodo_saida = periodo_saida_and_reason[0]
        self.reason = ' '.join(periodo_saida_and_reason[1].split())
    def set_periodo_ingresso(self,periodo_ingresso):
        self.periodo_ingresso = periodo_ingresso
    def set_ano_equivalencia(self,ano_equivalencia):
        self.ano_equivalencia = ano_equivalencia
    def set_coeficiente_rendimento(self,coeficiente_rendimento):
        self.coeficiente_rendimento = coeficiente_rendimento
    def set_curriculo(self,curriculo):
        self.curriculo = curriculo
    def set_carga_horaria_total(self,carga_horaria_total):
        self.carga_horaria_total = carga_horaria_total

#        self.reason = reason
        
class Field_miner():
    def extract_matricula(self,line):
        matricula = ''
        length = len('Matrcula:')
        index = line.find('Matrcula:')
        linha = line
        #print "#"+linha
        #Matricula first digit begins in position index + lenght + 1
        start_index = index + length + 1
        continua = True
        while continua: #Did not begin the Nascimento field
            matricula += line[start_index]
            start_index += 1
            try:
                if line[start_index] == 'N':
                    continua = False
            except IndexError:
                continua = False

        #print "Matr. "+matricula
        return matricula
    def extract_data_nascimento(self,line):
        data_nascimento = ''
        length = len('Nascimento:')
        index = line.find('Nascimento:')
        #Data Nascimento first digit begins in position index + length + 1
        start_index = index + length + 1
        for character in line[start_index:-1]: #Data Nascimento is the last word appearing on the given line 
            data_nascimento += character
        data_nascimento += line[-1]
        return data_nascimento
    def extract_naturalidade(self,line):
        naturalidade = ''
        length = len('Naturalidade:')
        index = line.find('Naturalidade:')
        start_index = index + length + 1
        #line[start_index] != 'R' and line[start_index + 1] != 'G' this doesnt allow Rio de Janeiro to be tracked
        while line[start_index + 2] != ':': #RG: is the next field
            naturalidade += line[start_index]
            start_index += 1
        return naturalidade
    def extract_nacionalidade(self,line):
        nacionalidade = ''
        length = len('Nacionalidade:') 
        index = line.find('Nacionalidade:')
        start_index = index + length + 1
        for character in line[start_index:-1]: #Nacionalidade is the last word appearing on the given line
            nacionalidade += character
        nacionalidade += line[-1]
        return nacionalidade
    def extract_course(self,line):
        '''Returns a list containing as its elements a given course information. E.g.: [MATA01,GEOMETRIA ANALITICA, 68, --, OB, 7,0 , AM ]'''
#        import unicodedata
        
#        def strip_accents(s):
#           return ''.join(c for c in unicodedata.normalize('NFD', s)
#                          if unicodedata.category(c) != 'Mn')
                          
        #line = line.encode('ascii',errors='ignore')
        is_intensive = False
        if '*' in line:
            is_intensive = True
            line = ''.join(line.split('*')[1:])
        string = line.split("                ")      
     
        result = []
        left_side = string[0].split(" ") #make use of the long sequence of white space to know when the course name ends and put on the left side
        
        regex_match = re.search('\d\d\d\d-\d',left_side[0]) #Removes the lines where the period appears         

        if regex_match:
            #result.append(regex_match.group(0))  #I already index by semester outside, this is redundant and make the list format inconsistent  
                      
            left_side = left_side[1:]               
        right_side = string[-1].split(" ") #put the remaining righten side of the course names on this string

#        print string[-1]
#        print left_side
#        print string
#        print left_side
        course_name = ''
        for element in left_side:
            if element != '':
                if len(result) >= 1:
#                    pass
#                    print element
                    course_name += element+' ' #this merges the course name, note i am adding an extra space to the end but given the crazy codes i think this wont matter
                else:
                    result.append(element)
#                print len(result)   
        course_name = course_name.rstrip()
        result.append(course_name)
#            print result,             
#            print len(result)         
#            if len(result) > 2:
#                course_name += element+' '
#            print course_name
#        print result
        for element in right_side:
            if element != '':
                result.append(element)                
   
        result[-1] = result[-1][0]+result[-1][1] # remove the \r and \n that always appear together 
  #        print result                             
 
        result.append(is_intensive)
        return result
    def extract_periodo_saida(self,line):
        
        periodo_saida = ''
        length = len('Sada:')
        index = line.find('Sada:')
        start_index = index + length + 1
        for character in line[start_index:-1]: #Saida is the last word appearing on the given line
            periodo_saida += character
        periodo_saida += line[-1]
        periodo_match = re.search('\d\d\d\d-\d',line[start_index:-1])
        if(periodo_match): #The student left the course at this point and the period and the REASON is anottated here.
            periodo_saida =  periodo_match.group(0)
            reason = line.split('/')[-1] #The reason and the period are separated by / as far as I noticed this always occur and is the last element
            return periodo_saida.rstrip(), reason.rstrip()
        else: return '','' # This is the case 'Saida: ' where the student didnt leave yet the course and the values are all empty.
#        print periodo_saida.rstrip()
#        print reason.rstrip()
#        sys.exit(0)
#        return periodo_saida
    def extract_periodo_ingresso(self,line):
        length = len('Ingresso:')
        index = line.find('Ingresso:')
        start_index = index + length + 1
        line = line[start_index:-1]
        line = line.split("     ")
#        line = line.split("/")
        periodo_ingresso = line[0]
        return periodo_ingresso
        

    def extract_irregular_enrollment(self, line):
        line = line.split('--')[0]
        line = line.split(' ')[2:-1]
        
        result = ''
#        print range(len(line) -1 )
        for element in line:
            if element != '':
                result += element+' '
#        print result.split()
        result = result[0:-1] #remove extra ending space
        return [result,'--','--','--','--','--']
    def extract_ano_equivalencia(self,line):
        line = line.split("    ")
        for element in line:
            if 'Ano' in element:
#                print element.split(':')[-1].rstrip()
                return element.split(':')[-1].rstrip()
    def extract_coeficiente_rendimento(self,line):
        return line.split("    ")[-1].split(":")[-1]
    def extract_curriculo(self,line):
        return  line.split("    ")[0].split(":")[-1]
    def extract_carga_horaria_total(self,line):    
        '''This should not be used. The CH often does not appear for classes the student taken. A different table should be used'''

def listFiles(path = sys.path[0]+"/arquivos/"):
    import os
    i = 0
    lista = os.listdir(path)
    listaArquivos = []
    for arquivo in os.listdir(path):
        extensao = arquivo[-4:]
        if(extensao == ".txt"):
            listaArquivos.append(path+arquivo)

    return listaArquivos        

if __name__=="__main__":
    
    listaArquivos = listFiles()

    for arquivo in listaArquivos:

        field_miner = Field_miner()
        
        filename = arquivo
        #pdf_text = pdf_to_csv(filename)
        
        #text_lines = pdf_text.split('\n')
        with open(filename) as f:
            text_lines = f.readlines()
        bla = ''
        for idx,line in enumerate(text_lines):
            for idj,char in enumerate(line):
              #  char.encode()
                if char != '\xff' and char != '\xfe' and char != '\x00' and char !='\xed':
                    bla += char
            text_lines[idx] = bla
            bla = ''
    #    print text_lines[1]
    #    sys.exit(0)
    #    from unidecode import unidecode
    #    print text_lines
    #    for idx,value in enumerate(text_lines):
    #        text_lines[idx] = text_lines[idx].encode('ascii', "ignore")
    #    for idx,value in enumerate(text_lines):
    #        print
        alunos = {}
        aluno = None #This is necessary so I know which is the student that I am currently extracting the data from the pdf transcript
        within_period = False #Used for checking if line contains a given period course information
        period = False #This is a flag indicating that the word Periodo occurred on a previous line so that we can know the next lines will list courses 
        for line in text_lines:

            if 'Aluno:' in line: #This is the 'Aluno' line. For each student, this is the first relevant information on a given page we are interested into.
    #            print 'test 1'
                matricula = field_miner.extract_matricula(line).rstrip()
    #            print matricula
                if matricula not in alunos: #The Aluno, Data Nascimento, among other information repeats for student idenfication when it spawns on multiple pages. No need to re-add them.
    #                print matricula
    #                print line
                    aluno = Aluno(matricula)
                    alunos[aluno.matricula] = aluno
    #            sys.stdout.write(matricula)
    #            print line
    #            print alunos[matricula]
    #            print alunos                
                    
                alunos[aluno.matricula].set_data_nascimento(field_miner.extract_data_nascimento(line))
                
            if 'Naturalidade' in line: #This is the 'Naturalidade line'.             
    #            print 'test 2'        
                aluno.set_naturalidade(field_miner.extract_naturalidade(line)) #This is part of the header so it will repeat, but since its the same value I will just reassign
                aluno.set_nacionalidade(field_miner.extract_nacionalidade(line)) #This is part of the header so it will repeat, but since its the same value I will just reassign
            
            if 'Curso:' in line: #This is the 'Curso line'.             
                curso_codigo = field_miner.extract_course(line)

            if 'Ingresso:' in line:
                pass
                aluno.set_periodo_ingresso(field_miner.extract_periodo_ingresso(line))
            
            if 'Sada:' in line:
                aluno.set_periodo_saida(field_miner.extract_periodo_saida(line))

            if 'Ano de equival' in line:
                aluno.set_ano_equivalencia(field_miner.extract_ano_equivalencia(line))
                
            if 'Coeficiente de Rendimento' in line:
                aluno.set_coeficiente_rendimento(field_miner.extract_coeficiente_rendimento(line))
                
            if 'Currculo:' in line:
                aluno.set_curriculo(field_miner.extract_curriculo(line))
                
    #        if 'CONDICIONAL' in line: 
                 #condicional is just to avoid for now the sem inscricao em disciplina and the associated line below it saying MATRICULA CONDICIONAL (SEE 20081 FIRST STUDENT)
    #            print 'test 3'
    #            continue # i agudo is \xed
            
            if 'Perodo'in line: 
    #            print 'test 4'            
                period = True
            
            if 'Total Geral' in line:
                #   This should not be used. The CH often does not appear for classes the student taken. A different table should be used
                pass
    #            aluno.set_carga_horaria_total(field_miner.extract_carga_horaria_total(line))
            period_match = re.search('\d\d\d\d-\d',line)
            if period_match and period: 
    #            print 'test 5'            
                current_line_period = period_match.group(0)
    #            print current_line_period
            
            if 'Legendas:' in line or 'Sub Total' in line: #After the period listing starts, it only ends when the 'Legendas' word appears. It will remain false until the next period line occurs.
    #            print 'test 6'        
                within_period = False
                period = False
            if (period_match and period) or within_period: #Test if the line has any number of the format <digit><digit><digit>-<digit> ; a number like 2010-1 should match.
    #            print 'test 7'
                within_period = True # After the first match period, the next time it enters this if it will be the same period if continue in a dif page or a dif period otherwise.            
    #            print 'outside'
    #            print aluno.matricula,
    #            print alunos[aluno.matricula].matricula, 
    #            print current_line_period            
    #            print alunos[aluno.matricula].courses.keys()
                if current_line_period not in alunos[aluno.matricula].courses: #A given student period information has not been added yet, initiate a list for adding the courses for that period
    #                print 'test 8'            
    #                print 'NAO TA ENTRANDO AQUI TEM QUE VER ISSO'
                    
     #               print 'inside'
    #                print aluno.matricula,
    #                print alunos[aluno.matricula].matricula, 
    #                print current_line_period            
    #                print alunos[aluno.matricula].courses.keys()
    #                alunos[aluno.matricula].courses[current_line_period] = [] #It is a dictionary since the semester is unique for every student (e.g. he cant have 2009.2 twice)
                    alunos[aluno.matricula].course_init(current_line_period)
              #  if 'CONDICIONAL' in line: print line
    #            print matricula
    #            print line
                if line[0] != '\r' and line[1] != '\n':  #end of page
    #                print aluno.matricula,

    #                print current_line_period
    #                alunos[aluno.matricula].courses[current_line_period] = field_miner.extract_course(line) # Adds the course of a given period to its respective semester
    #                print aluno.matricula
    #                print current_line_period
    #                print field_miner.extract_course(line)
    #                print alunos
    #                print alunos[aluno.matricula]
    #                print line.split(" ")[10]
    #                print line.split(" ")[9]

    #                print aluno.matricula
     #           print line.split(" ")
                    if all(item == '' for item in line.split(" ")[2:9]) and re.search('\d\d\d\d-\d',line) :# print line.split(" ") #Then something happened and no enrollment for the given semester ocurred
                        alunos[aluno.matricula].add_course(current_line_period,field_miner.extract_irregular_enrollment(line))                
    #                    print current_line_period
    #                    print line.split(" ")                    
    #                    print 'PRECISA VER O CASO DO 200810604 EM QUE OCORRE TRANCAMENTO TOTAL E AS DISCIPLINAS AINDA ASSIM APARECEM COM ESTADO DE TRANCADAS'
                    elif 'CONDICIONAL'in line:
                        alunos[aluno.matricula].add_course(current_line_period,['MATRICULA CONDICIONAL - S 6 - Art. 74 do REG','--','--','--','--','--'])
                    else:
                        alunos[aluno.matricula].add_course(current_line_period,field_miner.extract_course(line))
    #                print current_line_period,
    #                print alunos[aluno.matricula].courses
    #            print
    #            print
    #            print
            # print line
        curso_codigo = curso_codigo[1].split("-")[0]
        #at this point all student information should be held in memory and mined away from the pdf  
    #    sys.exit(0)

    #    for semestre,courses in alunos['200810601'].courses.iteritems():
    #        print semestre
    #        for course in courses:
    #            print '\t\t',
    #            print course    
    #            print
    #        print        
    #    sys.exit(0)      
        import numpy
    #    for matricula, aluno in alunos.iteritems():    
    #        print aluno.matricula.rstrip(),
    #        print aluno.data_nascimento.rstrip(),
    #        print aluno.naturalidade.rstrip(),
    #        print aluno.nacionalidade.rstrip(),
    #        print 'Curriculo: ',
    #        print aluno.curriculo.rstrip(),
    #        print 'Ano de Equivalencia: ',
    #        print aluno.ano_equivalencia,
    #        print 'Periodo Ingresso: ',
    #        print aluno.periodo_ingresso.rstrip(),
    #        print 'Periodo Saida: ',
    #        print aluno.periodo_saida.rstrip(),
    #        print 'CR: ',
    #        print aluno.coeficiente_rendimento.rstrip(),
    #        print aluno.reason.rstrip()
    #        print aluno.courses
            
    #    sys.exit(0)
    #        nota_semestre = []
    #        for semestre,courses in aluno.courses.iteritems():
    #            print 'Semestre: ',
    #            print semestre
    #            for course in courses:
    #                print '\t\t',
    #                print course    
    #                if  re.search('\d,\d',course[-2]):
    #                    course[-2] = course[-2][-3]+'.'+course[-2][-1]
    #                    nota_semestre.append(float(course[-2]))

    #            print 'Nota no Semestre: ',
    #            if not numpy.isnan(numpy.mean(nota_semestre)):
    #                print numpy.mean(nota_semestre),
    #            else:
    #                print None,
    #            nota_semestre = []
    #            print semestre.rstrip().split('-')[0] + semestre.rstrip().split('-')[1]
    #            for course in courses:
    #                if all(element == '--' for element in course[-5:-1]) and course[-1] == '--' and len(course) <=6:
    #                    print aluno.matricula
    #                    print '\t\t',
    #                    print course
    #            print
    #        print
    #    sys.exit(0)
    #   print alunos
    #   print len(alunos)
        import psycopg2
        with open(sys.path[0]+'/credentials.txt') as f:
            credentials = f.readlines()

        username = credentials[0].rstrip()
        password = credentials[1].rstrip()
        print curso_codigo
        #Connect to database
        try:
            conn = psycopg2.connect("dbname='proufba' user='"+username+"' host='localhost' password='"+password+"'")
        except:
            print "I am unable to connect to the database"

        cur = conn.cursor()
    #    curso_codigo = 195140

        #Insert curso related information
        try:
            cur.execute("INSERT INTO curso(codigo,nome) VALUES (%s,%s)",(curso_codigo,'Ciencia da Computacao'))
        except psycopg2.IntegrityError,exc:
            print exc
            pass
        conn.commit()
        #Insert curriculo related information        
        for matricula, aluno in alunos.iteritems():

            periodo_criado = aluno.curriculo.rstrip().split('-')[0]+aluno.curriculo.rstrip().split('-')[1]
            try:
                cur.execute("INSERT INTO curriculo(curso_codigo,periodo_criado) VALUES (%s,%s)",(curso_codigo,periodo_criado))
                
            except psycopg2.IntegrityError,exc:
                print exc        
                pass
            conn.commit()            
        #Insert aluno related information
        for matricula, aluno in alunos.iteritems():
    #        curso_codigo = 195140
            periodo_criado = aluno.curriculo.rstrip().split('-')[0]+aluno.curriculo.rstrip().split('-')[1]   
            if  aluno.coeficiente_rendimento.rstrip() != '':
                coeficiente_rendimento = aluno.coeficiente_rendimento.rstrip()
                if ',' in coeficiente_rendimento:
                    coeficiente_rendimento = coeficiente_rendimento.split(',')[0]+'.'+coeficiente_rendimento.split(',')[1]
            else:
                coeficiente_rendimento =  None
            ano_equivalencia = aluno.ano_equivalencia.rstrip().split('-')[0] + aluno.ano_equivalencia.rstrip().split('-')[1]
            if aluno.periodo_saida == '':
                periodo_saida = None
            else:
                periodo_saida = aluno.periodo_saida.split('-')[0]+aluno.periodo_saida.split('-')[1] 
            if aluno.reason == '':
                motivo_saida = None
            else:
                motivo_saida = aluno.reason.decode('iso-8859-1').encode('utf-8')

            data = aluno.data_nascimento.rstrip().rsplit('/')

            #print "DN "+aluno.data_nascimento.rstrip()
            #Add by Marcos MASS
            #if Date is not found create a default date
            if len(data) != 3:
                data = "01/01/1900"
                data = data.rstrip().rsplit('/')

            #print len(data)
            dia = data[0]
            mes = data[1]
            ano = data[2]
            data_nascimento = psycopg2.Date(int(ano),int(mes),int(dia))

            nac = aluno.nacionalidade.rstrip().decode('iso-8859-1').encode('utf-8') 
            naturalidade = aluno.naturalidade.decode('iso-8859-1').encode('utf-8').rsplit("RG")
            nacionalidade = nac[0]

            '''
            print "Matr. "+aluno.matricula.rstrip()
            print "CCodigo "+curso_codigo
            print "Periodo "+periodo_criado
            print "CR "
            print numpy.float64(coeficiente_rendimento)
            print "Ano "
            print ano_equivalencia
            print "Saida "
            print periodo_saida
            print "Motivo "
            print motivo_saida
            print "DN "
            print data_nascimento
            print "Nat. "
            print naturalidade
            print "Nac. "
            print nacionalidade
            '''

            try:            
                cur.execute("INSERT INTO aluno(matricula,curriculo_curso_codigo,curriculo_periodo_criado,coeficiente_rendimento,ano_equivalencia,periodo_saida,motivo_saida,data_nascimento,naturalidade,nacionalidade) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(aluno.matricula.rstrip(),curso_codigo,periodo_criado,numpy.float64(coeficiente_rendimento),ano_equivalencia,periodo_saida,motivo_saida,data_nascimento,naturalidade,nacionalidade))
            except psycopg2.IntegrityError,exc:
                print exc
                pass
            conn.commit()  
        #Insert periodo related information
        for matricula, aluno in alunos.iteritems():    
            for semestre,courses in aluno.courses.iteritems():
                nota_semestre = []
                table_semestre = semestre.rstrip().split('-')[0] + semestre.rstrip().split('-')[1]
                semester_is_irregular = False
                irregularity = None
                reason = None
                for course in courses: #to calculate the average semester grade
                    if  re.search('\d,\d',course[-3]):
                        course[-3] = course[-3][-3]+'.'+course[-3][-1]
    #                    print float(course[-2])
                        nota_semestre.append(float(course[-3]))
                        
                    if all(element == '--' for element in course[-5:-1]) and len(course) <=7: #This pattern occurs only when the request is irregular (note that regular request CAN contain a given class that was droped out)
                        semester_is_irregular = True
                        if irregularity == None:
                            irregularity = course[0].decode('iso-8859-1').encode('utf-8') #it is the first value that comes in a given semester if it occurs
                        elif reason == None:
                            reason = course[0].decode('iso-8859-1').encode('utf-8') #The reason comes after the irregularity, IF IT IS GIVEN AT ALL
                if not numpy.isnan(numpy.mean(nota_semestre)):
                    nota_semestre = numpy.mean(nota_semestre)
                else:
                    nota_semestre = None 
                try:
                    '''
                    print aluno.matricula.rstrip()
                    print table_semestre
                    print nota_semestre
                    print irregularity
                    print reason
                    ''' 
                    if semester_is_irregular:
                        cur.execute("INSERT INTO periodo(aluno_matricula,semestre,media_nota,situacao_periodo,detalhes) VALUES (%s,%s,%s,%s,%s)",(aluno.matricula.rstrip(),table_semestre,nota_semestre,irregularity,reason))
                    else:
                        cur.execute("INSERT INTO periodo(aluno_matricula,semestre,media_nota,situacao_periodo,detalhes) VALUES (%s,%s,%s,%s,%s)",(aluno.matricula.rstrip(),table_semestre,nota_semestre,'Regular',reason))
                        
                except psycopg2.IntegrityError,exc:
                    print exc        
                    pass
                conn.commit()
        #Insert periodo_contem_disciplina information
        for matricula, aluno in alunos.iteritems():    
            for semestre,courses in aluno.courses.iteritems():
                table_semestre = semestre.rstrip().split('-')[0] + semestre.rstrip().split('-')[1]
                for course in courses: #to calculate the average semester grade
                    if  re.search('\d,\d',course[-3]):
                        course[-3] = course[-3][-3]+'.'+course[-3][-1]
        #                    print float(course[-2])
                        nota_semestre.append(float(course[-3]))
                    
                    if all(element == '--' for element in course[-5:-1]) and len(course) <=7: #This pattern occurs only when the request is irregular (note that regular request CAN contain a given class that was droped out)
                        pass
                    else:
                        disciplina_codigo = course[0]
                        nota = None
                        if course[-3] != '--':
                            nota = course[-3]
                            #print nota
                            try:
                                nota = float(nota)
                            except ValueError:
                                nota = -1

                        resultado = None
                        if course[-2] != '--':
                            resultado = course[-2]
                        course_name = course[1][0:40].decode('iso-8859-1').encode('utf-8')
                        '''
                        #disciplina
                        ch_disciplina = -1 #This is a reminder that ch_disciplina sometimes is missing and needs to add another table in. The pdf is wrong on its own right.
                        if course[-6] != '--':
                            ch_disciplina = course[-6]
                        try:             
                            cur.execute("INSERT INTO disciplina(codigo,nome,carga_horaria) VALUES (%s,%s,%s)",(disciplina_codigo,course_name,ch_disciplina))
                        except psycopg2.IntegrityError,exc:
                               # print exc
                           pass
                        conn.commit()
                        '''
                        #periodo_contem_disciplina
                        e_itensivo = course[-1]
                          #  print e_itensivo
                        try:
                            cur.execute("INSERT INTO periodo_contem_disciplina(periodo_semestre,periodo_aluno_matricula,disciplina_codigo,nota,resultado,e_itensivo) VALUES (%s,%s,%s,%s,%s,%s)",(table_semestre,aluno.matricula.rstrip(),disciplina_codigo,nota,resultado,e_itensivo))
                        except psycopg2.IntegrityError,exc:
                            print exc
                            pass
                        conn.commit()   
                        
    #                try:            
    #                    cur.execute("INSERT INTO periodo(aluno_matricula,semestre,media_nota,tipo_requisicao,justificativa) VALUES (%s,%s,%s,%s,%s)",(aluno.matricula.rstrip(),table_semestre))
     #               except psycopg2.IntegrityError,exc:
     #                   print exc        
     #               conn.commit()
    #    for aluno in alunos:
    #        print aluno.matricula
    #        print aluno.courses
    #            for 
    #            print line[index + length + 1]
    #            print line[index + length +2]
    #            print line[index + length +3]            
    #            print line[index + length +4]            
    #            print line[index + length +5]            
    #            print line[index + length +6]            
    #    print pdf_text
