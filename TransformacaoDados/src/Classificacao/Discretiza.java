/*
begin;
drop table aluno_semestre_si_discretizado;
create table aluno_semestre_si_discretizado as select * from aluno_semestre_si;
ALTER TABLE aluno_semestre_si_discretizado alter column idade type varchar(45);
ALTER TABLE aluno_semestre_si_discretizado alter column coeficiente_rendimento type varchar(45);
ALTER TABLE aluno_semestre_si_discretizado alter column carga_horaria type varchar(45);
ALTER TABLE aluno_semestre_si_discretizado alter column quantidade_cursada type varchar(45);
ALTER TABLE aluno_semestre_si_discretizado alter column quantidade_di type varchar(45);
ALTER TABLE aluno_semestre_si_discretizado alter column quantidade_du type varchar(45);
ALTER TABLE aluno_semestre_si_discretizado alter column quantidade_reprovado type varchar(45);
ALTER TABLE aluno_semestre_si_discretizado alter column quantidade_reprovado_falta type varchar(45);
ALTER TABLE aluno_semestre_si_discretizado alter column quantidade_aprovado type varchar(45);
ALTER TABLE aluno_semestre_si_discretizado alter column quantidade_trancada type varchar(45);
ALTER TABLE aluno_semestre_si_discretizado alter column quantidade_optativa type varchar(45);
ALTER TABLE aluno_semestre_si_discretizado alter column quantidade_obrigatoria type varchar(45);
commit;


begin;

--Coeficiente de Rendimento
update aluno_semestre_si_discretizado
set coeficiente_rendimento = 'EXCELENTE'
where coeficiente_rendimento >='9';

update aluno_semestre_si_discretizado
set coeficiente_rendimento = 'BOM'
where coeficiente_rendimento >='7' and coeficiente_rendimento <='8.9';

update aluno_semestre_si_discretizado
set coeficiente_rendimento = 'REGULAR'
where coeficiente_rendimento >='5' and coeficiente_rendimento <='6.9';

update aluno_semestre_si_discretizado
set coeficiente_rendimento = 'RUIM'
where coeficiente_rendimento >='3' and coeficiente_rendimento <='4.9';

update aluno_semestre_si_discretizado
set coeficiente_rendimento = 'PESSIMO'
where coeficiente_rendimento <='2.9';

--Carga Horaria
update aluno_semestre_si_discretizado
set carga_horaria = 'ALTA'
where carga_horaria >='401' and carga_horaria <='600';

update aluno_semestre_si_discretizado
set carga_horaria = 'MEDIA'
where carga_horaria >='151' and carga_horaria <='400';

update aluno_semestre_si_discretizado
set carga_horaria = 'BAIXA'
where carga_horaria <='150';

--Quantidade Cursada
update aluno_semestre_si_discretizado
set quantidade_cursada = 'GRANDE'
where quantidade_cursada >='5';


update aluno_semestre_si_discretizado
set quantidade_cursada = 'MEDIA'
where quantidade_cursada >='3' and quantidade_cursada <='4';

update aluno_semestre_si_discretizado
set quantidade_cursada = 'BAIXA'
where quantidade_cursada <='2';


--Quantidade Reprovados

update aluno_semestre_si_discretizado
set quantidade_reprovado = 'GRANDE'
where quantidade_reprovado >='5';

update aluno_semestre_si_discretizado
set quantidade_reprovado = 'MEDIA'
where quantidade_reprovado >='3' and quantidade_reprovado <='4';

update aluno_semestre_si_discretizado
set quantidade_reprovado = 'BAIXA'
where quantidade_reprovado <='2';


--Quantidade Aprovado

update aluno_semestre_si_discretizado
set quantidade_aprovado = 'GRANDE'
where quantidade_aprovado >='5';

update aluno_semestre_si_discretizado
set quantidade_aprovado = 'MEDIA'
where quantidade_aprovado >='3' and quantidade_aprovado <='4';

update aluno_semestre_si_discretizado
set quantidade_aprovado = 'BAIXA'
where quantidade_aprovado <='2';


--Quantidade Obrigatória

update aluno_semestre_si_discretizado
set quantidade_obrigatoria = 'GRANDE'
where quantidade_obrigatoria >='5';

update aluno_semestre_si_discretizado
set quantidade_obrigatoria = 'MEDIA'
where quantidade_obrigatoria >='3' and quantidade_obrigatoria <='4';

update aluno_semestre_si_discretizado
set quantidade_obrigatoria = 'BAIXA'
where quantidade_obrigatoria <='2';


--Idade dos alunos
update aluno_semestre_si_discretizado
set idade = 'ADULTO'
where idade >='29';

update aluno_semestre_si_discretizado
set idade = 'JOVEM'
where idade >='23' and idade <='28';

update aluno_semestre_si_discretizado
set idade = 'ADOLESCENTE'
where idade <='22';



*/

