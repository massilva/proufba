package Classificacao;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JOptionPane;

import Conexao.Connect;


public class TransformacaoToClassificacao {
	private int cursoCodigo;
	private int curriculoPeriodo;
	private String siglaCurso;
	private Connect conecta;
	private int aluno;
	private int semestreCorrente;
	private Statement executa;
	
	
	
	TransformacaoToClassificacao(int cursoCodigo, int curriculoPeriodo, Connect conecta, String siglaCurso) throws SQLException {
		setCurriculoPeriodo(curriculoPeriodo);
		setCursoCodigo(cursoCodigo);
		setConecta(conecta);
		setSiglaCurso(siglaCurso);
		semestreCorrente = 1;
		executa = getConecta().getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
	}
	
	public void Classificacao(){
		Statement executaUpdate = null;
		excluiTabela();
		criaTabela();		
		
		try{
			executaUpdate = getConecta().getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			String sql = "select matricula,semestre_relativo,nacionalidade,naturalidade,data_nascimento from aluno join periodo on aluno_matricula = matricula where curriculo_curso_codigo = "+getCursoCodigo()+" and curriculo_periodo_criado = "+getCurriculoPeriodo()+" order by (matricula,semestre_relativo)";
			ResultSet alunos = executaUpdate.executeQuery(sql);
			int idade = 2; //MUDAR
			char sexo;
			while(alunos.next()){
				setAluno(alunos.getInt("matricula"));
				setSemestreCorrente(alunos.getInt("semestre_relativo"));
				if(alunos.getString("nacionalidade").equalsIgnoreCase("Brasileira")) sexo = 'F'; else sexo = 'M';
				idade = getIdade(alunos.getDate("data_nascimento"));
				 sql = "insert into aluno_semestre_"+siglaCurso+" (matricula,semestre_relativo,idade,sexo,nacionalidade,naturalidade,coeficiente_rendimento,carga_horaria,quantidade_cursada,quantidade_di,quantidade_du," +
						"quantidade_reprovado,quantidade_reprovado_falta,quantidade_trancada,quantidade_aprovado,quantidade_intensivo,quantidade_optativa,quantidade_obrigatoria)" +
						" values ("+getAluno()+","+getSemestreCorrente()+","+idade+",'"+sexo+"','"+alunos.getString("nacionalidade")+"'" +
								",'"+alunos.getString("naturalidade")+"',"+getCoeficienteDeRendimento()+","+getCargaHoraria()+","+getQuantidadeDisciplinaCursada()+","+getQuantidadeDisciplinasDI()+","+getQuantidadeDisciplinasDU()+"" +
								","+getQuantidadeDisciplinasReprovado()+","+getQuantidadeDisciplinasReprovadoFalta()+","+getQuantidadeDisciplinasTrancadas()+"" +
								","+getQuantidadeDisciplinasAprovado()+","+getQuantidadeDisciplinasIntensivo()+","+getQuantidadeDisciplinaOptativa()+"" +
								","+getQuantidadeDisciplinaObrigatoria()+");";
				getExecuta().executeUpdate(sql);
				
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
	public  int getIdade(Date data) {  
        Calendar cData = Calendar.getInstance();  
        Calendar cHoje= Calendar.getInstance();  
        cData.setTime(data);  
        cData.set(Calendar.YEAR, cHoje.get(Calendar.YEAR));  
        int idade = cData.after(cHoje) ? -1 : 0;  
        cData.setTime(data);  
        idade += cHoje.get(Calendar.YEAR) - cData.get(Calendar.YEAR);  
        return idade;  
    }  
	

	public Statement getExecuta (){
		return this.executa;
	}

	private void excluiTabela(){
		try{
			String sql = "drop table  aluno_semestre_"+getSiglaCurso()+"";
			getExecuta().executeUpdate(sql);
		}
		catch (Exception e){
		}
	}


	private void criaTabela(){
		try{
			String sql = "create table aluno_semestre_"+getSiglaCurso()+"( " +
					"matricula integer, " +
					"retencao boolean, " +
					"semestre_relativo integer," +
					"idade integer," +
					"sexo char," +
					"nacionalidade varchar(30)," +
					"naturalidade varchar(30)," +
					"coeficiente_rendimento float," +
					"carga_horaria integer," +
					"quantidade_cursada integer," +
					"quantidade_di integer," +
					"quantidade_du integer," +
					"quantidade_reprovado integer," +
					"quantidade_reprovado_falta integer," +
					"quantidade_trancada integer," +
					"quantidade_aprovado integer," +
					"quantidade_intensivo integer," +
					"quantidade_optativa integer," +
					"quantidade_obrigatoria integer," +
					"primary key (matricula,semestre_relativo));";
			getExecuta().executeUpdate(sql);
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
	
	//TEM QUE MUDAR O SEMESTRE
	private int getQuantidadeDisciplinaCursada(){
		int dado = 0;
		String sql =  "select periodo_aluno_matricula,count(*) as quantidade_disciplina_cursada" +
				" from periodo_contem_disciplina join periodo on periodo_semestre = semestre  and periodo_aluno_matricula = aluno_matricula join aluno on aluno_matricula = matricula " +
				" where semestre_relativo = "+getSemestreCorrente()+"  and curriculo_curso_codigo ="+getCursoCodigo()+" and curriculo_periodo_criado ="+getCurriculoPeriodo()+" and periodo_aluno_matricula="+getAluno()+" " +
				" group by periodo_aluno_matricula";
		try{
			ResultSet dados = getExecuta().executeQuery(sql);
			while(dados.next()) dado = dados.getInt("quantidade_disciplina_cursada");
		}catch(Exception e){e.printStackTrace();}
		
		return dado;
		
	}
	
	private int getQuantidadeDisciplinasAprovado(){
		int dado = 0;
		String sql =  "select periodo_aluno_matricula,count(*) as quantidade_disciplina_aprovado from periodo_contem_disciplina join periodo on periodo_semestre = semestre and periodo_aluno_matricula = aluno_matricula join aluno on aluno_matricula = matricula  " +
				"where curriculo_curso_codigo = "+getCursoCodigo()+" and curriculo_periodo_criado ="+getCurriculoPeriodo()+" and semestre_relativo = "+getSemestreCorrente()+" " +
				" and periodo_aluno_matricula="+getAluno()+" and (resultado= 'AP' or resultado= 'AA' or resultado= 'AM' or resultado='MF') " +
				" group by periodo_aluno_matricula";
		try{
			ResultSet dados = getExecuta().executeQuery(sql);
			while(dados.next()) dado = dados.getInt("quantidade_disciplina_aprovado");
		}catch(Exception e){e.printStackTrace();}
		
		return dado; 
	}
	
	private int getQuantidadeDisciplinasReprovado(){
		int dado = 0;
		String sql =  "select periodo_aluno_matricula,count(*) as quantidade_disciplina_reprovado from periodo_contem_disciplina join periodo on periodo_semestre = semestre and periodo_aluno_matricula = aluno_matricula join aluno on aluno_matricula = matricula  " +
				"where curriculo_curso_codigo = "+getCursoCodigo()+" and curriculo_periodo_criado ="+getCurriculoPeriodo()+" and semestre_relativo = "+getSemestreCorrente()+" " +
				" and periodo_aluno_matricula="+getAluno()+" and (resultado='RC' or resultado= 'RM' or resultado= 'RR') " +
				" group by periodo_aluno_matricula";
		try{
			ResultSet dados = getExecuta().executeQuery(sql);
			while(dados.next()) dado = dados.getInt("quantidade_disciplina_reprovado");
		}catch(Exception e){e.printStackTrace();}
		
		return dado; 
	}
	private int getQuantidadeDisciplinasReprovadoFalta(){
		int dado = 0;
		String sql =  "select periodo_aluno_matricula,count(*) as quantidade_disciplina_reprovado_falta from periodo_contem_disciplina join periodo on periodo_semestre = semestre and periodo_aluno_matricula = aluno_matricula join aluno on aluno_matricula = matricula  " +
				"where curriculo_curso_codigo = "+getCursoCodigo()+" and curriculo_periodo_criado ="+getCurriculoPeriodo()+" and semestre_relativo = "+getSemestreCorrente()+" " +
				" and periodo_aluno_matricula="+getAluno()+" and (resultado='RF') " +
				" group by periodo_aluno_matricula";
		try{
			ResultSet dados = getExecuta().executeQuery(sql);
			while(dados.next()) dado = dados.getInt("quantidade_disciplina_reprovado_falta");
		}catch(Exception e){e.printStackTrace();}
		
		return dado; 
	}
			
	
	private int getQuantidadeDisciplinasTrancadas(){
		int dado = 0;
		String sql =  "select periodo_aluno_matricula,count(*) as quantidade_disciplina_trancada from periodo_contem_disciplina join periodo on periodo_semestre = semestre and periodo_aluno_matricula = aluno_matricula join aluno on aluno_matricula = matricula  " +
				"where curriculo_curso_codigo = "+getCursoCodigo()+" and curriculo_periodo_criado ="+getCurriculoPeriodo()+" and semestre_relativo = "+getSemestreCorrente()+" " +
				" and periodo_aluno_matricula="+getAluno()+" and (resultado='TR') " +
				" group by periodo_aluno_matricula";
		try{
			ResultSet dados = getExecuta().executeQuery(sql);
			while(dados.next()) dado = dados.getInt("quantidade_disciplina_trancada");
		}catch(Exception e){e.printStackTrace();}
		
		return dado; 
	}
	
	private int getQuantidadeDisciplinasDI(){
		int dado = 0;
		String sql =  "select periodo_aluno_matricula,count(*) as quantidade_disciplina_di from periodo_contem_disciplina join periodo on periodo_semestre = semestre " +
				"and periodo_aluno_matricula = aluno_matricula join aluno on aluno_matricula = matricula  " +
				" where curriculo_curso_codigo = "+getCursoCodigo()+" and curriculo_periodo_criado = "+getCurriculoPeriodo()+" and semestre_relativo = "+getSemestreCorrente()+"  and periodo_aluno_matricula="+getAluno()+"  and (resultado= 'DI')" +
				" group by periodo_aluno_matricula";
		try{
			ResultSet dados = getExecuta().executeQuery(sql);
			while(dados.next()) dado = dados.getInt("quantidade_disciplina_di");
		}catch(Exception e){e.printStackTrace();}
		
		return dado; 
	}
	
	private int getQuantidadeDisciplinasDU(){
		int dado = 0;
		String sql = "select periodo_aluno_matricula,count(*) as quantidade_disciplina_du from periodo_contem_disciplina join periodo on periodo_semestre = semestre " +
				"and periodo_aluno_matricula = aluno_matricula join aluno on aluno_matricula = matricula  " +
				" where curriculo_curso_codigo = "+getCursoCodigo()+" and curriculo_periodo_criado = "+getCurriculoPeriodo()+" and semestre_relativo = "+getSemestreCorrente()+"  and periodo_aluno_matricula="+getAluno()+"  and (resultado= 'DU')" +
				" group by periodo_aluno_matricula";
		try{
			ResultSet dados = getExecuta().executeQuery(sql);
			while(dados.next()) dado = dados.getInt("quantidade_disciplina_du");
		}catch(Exception e){e.printStackTrace();}
		
		return dado; 
	}
	
	private int getQuantidadeDisciplinasIntensivo(){
		int dado = 0;
		String sql = "select periodo_aluno_matricula,count(*) as quantidade_disciplina_intensivo from periodo_contem_disciplina join periodo on periodo_semestre = semestre and periodo_aluno_matricula = aluno_matricula join aluno on aluno_matricula = matricula  " +
				"where curriculo_curso_codigo = "+getCursoCodigo()+" and curriculo_periodo_criado ="+getCurriculoPeriodo()+" and semestre_relativo = "+getSemestreCorrente()+" " +
				" and periodo_aluno_matricula="+getAluno()+" and e_itensivo = true" +
				" group by periodo_aluno_matricula";
		try{
			ResultSet dados = getExecuta().executeQuery(sql);
			while(dados.next()) dado = dados.getInt("quantidade_disciplina_intensivo");
		}catch(Exception e){e.printStackTrace();}
		
		return dado; 
	}
	
	private int getQuantidadeDisciplinaOptativa(){
		int dado = 0;
		String sql = "select periodo_aluno_matricula,count(*) as quantidade_optativa from curriculo_contem_disciplina natural join periodo_contem_disciplina join  " +
				" periodo on periodo_aluno_matricula = aluno_matricula and periodo_semestre = semestre " +
				"where curriculo_curso_codigo =  "+getCursoCodigo()+" and curriculo_periodo_criado = "+getCurriculoPeriodo()+" and semestre_relativo = "+getSemestreCorrente()+"  and natureza = 'OP' and periodo_aluno_matricula="+getAluno()+"" +
				" group by periodo_aluno_matricula";
		try{
			ResultSet dados = getExecuta().executeQuery(sql);
			while(dados.next()) dado = dados.getInt("quantidade_optativa");
		}catch(Exception e){e.printStackTrace();}
		
		return dado; 
	
	}
	
	private int getQuantidadeDisciplinaObrigatoria(){
		int dado = 0;
		String sql = "select periodo_aluno_matricula,count(*) as quantidade_obrigatoria from curriculo_contem_disciplina natural join periodo_contem_disciplina join  " +
				" periodo on periodo_aluno_matricula = aluno_matricula and periodo_semestre = semestre " +
				"where  curriculo_curso_codigo = "+getCursoCodigo()+" and curriculo_periodo_criado = "+getCurriculoPeriodo()+" and semestre_relativo = "+getSemestreCorrente()+"  and natureza = 'OB' and periodo_aluno_matricula="+getAluno()+"" +
				" group by periodo_aluno_matricula";
		try{
			ResultSet dados = getExecuta().executeQuery(sql);
			while(dados.next()) dado = dados.getInt("quantidade_obrigatoria");
		}catch(Exception e){e.printStackTrace();}
		
		return dado; 
		 
	}
	
	//Coeficiente de rendimento calculado por media aritmetica (soma das notas/quantidade de disciplinas)
	private double getCoeficienteDeRendimento(){
		int dado = 0;
		String sql = "select periodo_aluno_matricula,semestre, SUM(nota)/count(*)  as coeficiente_rendimento" +
				" from periodo_contem_disciplina join periodo on periodo_aluno_matricula = aluno_matricula and periodo_semestre = semestre join aluno on matricula = aluno_matricula" +
				" where curriculo_curso_codigo = "+getCursoCodigo()+" and curriculo_periodo_criado = "+getCurriculoPeriodo()+" and semestre_relativo = "+getSemestreCorrente()+"  and periodo_aluno_matricula="+getAluno()+"" +
				" group by periodo_aluno_matricula,semestre";
		try{
			ResultSet dados = getExecuta().executeQuery(sql);
			while(dados.next()) dado = dados.getInt("coeficiente_rendimento");
		}catch(Exception e){e.printStackTrace();}
		
		return dado; 
	
	}
	
	private int getCargaHoraria(){
		int dado = 0;
		String sql = "select periodo_aluno_matricula,semestre, SUM(carga_horaria) as carga_horaria from disciplina join periodo_contem_disciplina on disciplina_codigo = codigo join periodo on periodo_aluno_matricula = aluno_matricula and periodo_semestre = semestre join aluno on matricula = aluno_matricula " +
				"where curriculo_curso_codigo = "+getCursoCodigo()+" and curriculo_periodo_criado = "+getCurriculoPeriodo()+" and semestre_relativo = "+getSemestreCorrente()+"  and periodo_aluno_matricula="+getAluno()+"" +
				" and  (resultado= 'AP' or resultado= 'AA' or resultado= 'AM' or resultado='MF' or resultado='DI' or resultado='DU') " +
				"group by periodo_aluno_matricula,semestre";
		try{
			ResultSet dados = getExecuta().executeQuery(sql);
			while(dados.next()) dado = dados.getInt("carga_horaria");
		}catch(Exception e){e.printStackTrace();}
		
		return dado; 
		
	}
	
	
	
	private int getCursoCodigo() {
		return cursoCodigo;
	}

	private void setCursoCodigo(int cursoCodigo) {
		this.cursoCodigo = cursoCodigo;
	}
	
	private int getCurriculoPeriodo() {
		return curriculoPeriodo;
	}

	private void setCurriculoPeriodo(int curriculoPeriodo) {
		this.curriculoPeriodo = curriculoPeriodo;
	}
	
	private void setConecta(Connect conecta){
		this.conecta = conecta;
	}
	
	private Connect getConecta(){
		return conecta;
	}

	public int getAluno() {
		return aluno;
	}

	public void setAluno(int aluno) {
		this.aluno = aluno;
	}

	public String getSiglaCurso() {
		return siglaCurso;
	}

	public void setSiglaCurso(String siglaCurso) {
		this.siglaCurso = siglaCurso;
	}

	public int getSemestreCorrente() {
		return semestreCorrente;
	}

	public void setSemestreCorrente(int semestreCorrente) {
		this.semestreCorrente = semestreCorrente;
	}
	

}
