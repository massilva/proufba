import java.sql.ResultSet;
import java.sql.Statement;

import Conexao.Connect;

public class CopyOfTransformacaoToNominal {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Connect conecta = new Connect();
		if(conecta.conecta()){
			Statement executa = null;	
			Statement atualiza = null;
			try{
				executa = conecta.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				atualiza = conecta.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				String sql = " select a.attname as coluna from pg_catalog.pg_attribute a  inner join" +
						" pg_stat_user_tables c on a.attrelid = c.relid  " +
						"WHERE relname = 'aluno_disciplina_transformada_si' and a.attnum > 0 and a.attname!= 'periodo_aluno_matricula'" +
						" and a.attname!= 'periodo_semestre' and a.attname!= 'semestre_relativo' and attname!= 'retencao'" +
						" AND NOT a.attisdropped order by c.relname, a.attname ";
				ResultSet colunas = executa.executeQuery(sql);
				while(colunas.next()){
					String coluna = colunas.getString("coluna");
						atualiza.execute("begin;");
						sql = "update aluno_disciplina_transformada_si set "+coluna+" = 'APROVADO'" +
								" where "+coluna+"='DI' or "+coluna+"= 'DU' or "+coluna+"= 'AP' or "+coluna+"= 'AA' or "+coluna+"= 'AM' or "+coluna+"='MF' ";
						atualiza.execute(sql);
						sql = "update aluno_disciplina_transformada_si set "+coluna+" = 'REPROVADO FALTA'" +
								" where "+coluna+"='RF' ";
						atualiza.execute(sql);
						sql = "update aluno_disciplina_transformada_si set "+coluna+" = 'REPROVADO'" +
								" where "+coluna+"='RC' or "+coluna+"='RM' or "+coluna+"='RR' or "+coluna+"='TR'";
						atualiza.execute(sql);
						sql = "update aluno_disciplina_transformada_si set "+coluna+" = NULL" +
								" where "+coluna+"='null'";
						atualiza.execute(sql);
				}
				atualiza.execute("commit;");
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}

}
