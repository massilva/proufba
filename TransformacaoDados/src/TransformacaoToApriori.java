import java.io.ObjectInputStream.GetField;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

import Conexao.Connect;

public class TransformacaoToApriori {
	static Statement comando = null;
	private String disciplinasCursadas[] = new String[100];
	private int quantidadeDisciplinaCursadas[] = new int[100];
	private int cursoCodigo;
	private int curriculoPeriodo;
	private String disciplina;
	private String resultado;
	private Connect conecta;
	private String siglaCurso;
	private int posicaoDisciplina;
	private int posicaoDisciplinaCursada;
	private int alunoMatricula;
	private int periodoAluno;
	private boolean mudouPeriodo;
	private boolean mudouAluno;
	
	TransformacaoToApriori(int cursoCodigo, int curriculoPeriodo, String siglaCurso, Connect conecta){
		setCurriculoPeriodo(curriculoPeriodo);
		setCursoCodigo(cursoCodigo);
		setConecta(conecta);
		setSiglaCurso(siglaCurso);
		for (int i = 0; i < 100; i ++){
			quantidadeDisciplinaCursadas[i] = 1;
			disciplinasCursadas[i] = null;
		}
		setMudouPeriodo(false);
		setMudouAluno(false);	
		setPosicaoDisciplina(0);
		setPosicaoDisciplinaCursada(0);
	}
		
	
	public TransformacaoToApriori(int curriculoPeriodo, int cursoCodigo, Connect conecta, String siglaCurso) {
		setCurriculoPeriodo(curriculoPeriodo);
		setCursoCodigo(cursoCodigo);
		setConecta(conecta);
		setSiglaCurso(siglaCurso);
		comando = conecta.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		deletaTabela();
		criaTabela();
		try{
			String str = "select periodo_semestre,periodo_aluno_matricula,disciplina_codigo,nota,resultado,curriculo_curso_codigo " +
					"from periodo_contem_disciplina join aluno on periodo_aluno_matricula = matricula " +
					"where curriculo_curso_codigo = "+getCursoCodigo()+" and curriculo_periodo_criado = "+getCurriculoPeriodo()+" order by (periodo_aluno_matricula,periodo_semestre);";
			ResultSet dados_aluno = comando.executeQuery(str);
		
			if (dados_aluno != null){
				//Atualiza o primeiro aluno
				while(dados_aluno.next()){
					LeAluno(dados_aluno);
					
					atualizaTabela();
					setDisciplinasCursadas(getDisciplina());
					incrementaPosicaoDisciplina();
					break;
				}
				dados_aluno.absolute(1);
				while(dados_aluno.next()){
					//Verifico se o aluno ja cursou a disciplina
					//Se cursou sera incrementado mais 1 na disciplina que ele cursou
					setPosicaoDisciplinaCursada(jaCursouDisciplina(dados_aluno.getString("disciplina_codigo"))); 
					//Os alunos e periodos são iguais
					//Aluno ainda esta no mesmo semestre
					if((getPeriodoAluno() == dados_aluno.getInt("periodo_semestre")) && getAlunoMatricula() == dados_aluno.getInt("periodo_aluno_matricula")){
						//se o aluno nao cursou a disciplina
						if  (posicaoDisciplinaCursada != -1){
							//quantidade_disciplina_cursada[posicao_disciplina_cursada]++;
							LeAluno(dados_aluno);
							//quantidade_disciplina_cursada[posicao_disciplina_cursada],			
							atualizaTabela();		
						}
						//o aluno já cursou a disciplina
						else{
							atualizaTabela();
						//	disciplinas_cursadas[posicao_disciplina] = getDisciplina();
							//posicao_disciplina++;
						}
					}
					
					//O aluno foi para outro semestre
					else if((getPeriodoAluno()!=dados_aluno.getInt("periodo_semestre")) && getAlunoMatricula() == dados_aluno.getInt("periodo_aluno_matricula")){
						//se o aluno nao cursou a disciplina
						setMudouPeriodo(true);
						if  (posicaoDisciplinaCursada != -1){
												
							quantidadeDisciplinaCursadas[posicaoDisciplinaCursada]++;
							atualizaTabela();								
						}
						//o aluno já cursou a disciplina
						else {
							atualizaTabela();
							disciplinasCursadas[posicaoDisciplina] = getDisciplina();
							posicao_disciplina++;
						}
						setMudouPeriodo(false);
					}
					else{
						setMudouAluno(true);
						setMudouPeriodo(true);
						LeAluno(dados_aluno);
						atualizaTabela();
						for (int i = 0; i < 100; i ++){
							quantidadeDisciplinaCursadas[i] = 1;
							disciplinasCursadas[i] = null;
						}
						setMudouPeriodo(false);
						setMudouAluno(false);						
						setPosicaoDisciplina(0);
						setPosicaoDisciplinaCursada(0);
						disciplinas_cursadas[posicao_disciplina] = getDisciplina();
						posicao_disciplina++;
					}
				}
				
			}
		
		}
		catch (Exception e){
			e.printStackTrace();
		}
		adicionaSemestre();
		
		
	}


	private void LeAluno(ResultSet dados_aluno) throws SQLException {
		setAlunoMatricula(dados_aluno.getInt("periodo_aluno_matricula"));
		setPeriodoAluno(dados_aluno.getInt("periodo_semestre"));
		setDisciplina(dados_aluno.getString("disciplina_codigo"));
		setResultado(dados_aluno.getString("resultado"));
	}



	
	private int jaCursouDisciplina(String disciplina){
		int posicao = 0;
		while(getDisciplinasCursadas(posicao)!= null){
			if (disciplina.equals(getDisciplinasCursadas(posicao))){
				return posicao;
			}
			posicao++;
		}
		return -1;
	}
	
	
	private void deletaTabela() {
		try{
			comando.executeUpdate("drop table aluno_disciplina_transformada_"+getSiglaCurso()+";");
		}
		catch (Exception e){
			//Igonra erro, caso não exista a tabela
		}
		
	}


	private void criaTabela(){
		try{
			comando.executeUpdate("create table aluno_disciplina_transformada_"+getSiglaCurso()+"( " +
					"periodo_semestre integer, " +
					"periodo_aluno_matricula integer," +
					"semestre_letivo integer," +
					"retencao boolean," +
					"primary key (periodo_aluno_matricula,periodo_semestre));");
		//	comando.executeUpdate("alter table aluno_disciplina_transformada_"+curso+" add primary key (periodo_semestre,periodo_aluno_matricula);");
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void atualizaTabela(){
		Statement executa = null;
		boolean existe_disciplina = false;
		String disciplina_transformada = getDisciplina() +"dis"+getQuantidadeDisciplinasCursadas();
		try{
			executa = conecta.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ResultSet colunas_existentes = executa.executeQuery("select a.attname as "+"coluna"+" from pg_catalog.pg_attribute a  inner join pg_stat_user_tables c on a.attrelid = c.relid WHERE relname = 'aluno_disciplina_transformada_"+getCursoCodigo()+"' and a.attnum > 0 AND NOT a.attisdropped order by c.relname, a.attname");
			//Verifica as colunas existentes
			while(colunas_existentes.next()){
				String coluna = colunas_existentes.getString("coluna");
				if(coluna.equalsIgnoreCase(disciplina_transformada)){
					existe_disciplina = true;
					break;
				}
			}
			colunas_existentes.close();
			if(existe_disciplina == false)
				executa.executeUpdate("alter table aluno_disciplina_transformada_"+getSiglaCurso()+" add column "+disciplina_transformada+" varchar(15);");

			//Se mudou o período e o aluno, existe um novo aluno.
			//Insere o novo aluno e atualiza a tabela
			if((getMudouPeriodo() == true) && (getMudouAluno() == true)){
				String x = "insert into aluno_disciplina_transformada_"+getSiglaCurso()+" (periodo_semestre, periodo_aluno_matricula ) values ("+getPeriodoAluno()+","+getAlunoMatricula()+");";
				executa.executeUpdate(x);
				x = "update aluno_disciplina_transformada_"+getSiglaCurso()+" set "+disciplina_transformada+" = '"+getResultado()+"' where periodo_semestre = "+getPeriodoAluno()+" and periodo_aluno_matricula = "+ getAlunoMatricula()+";";
				executa.executeUpdate(x);
			}
			//O período do aluno mudou
			//Adiciona o aluno com o novo periodo
			else if (getMudouPeriodo() == true){
				String x = "insert into aluno_disciplina_transformada_"+getSiglaCurso()+" (periodo_semestre, periodo_aluno_matricula,"+disciplina +"dis"+getQuantidadeDisciplinasCursadas()+") values ("+getPeriodoAluno()+","+getAlunoMatricula()+",'"+getResultado()+"');";
				executa.executeUpdate(x);
			}
			//Atualiza os dados do aluno
			//O aluno e o período continuam o mesmo
			else{
				String x = "update aluno_disciplina_transformada_"+getSiglaCurso()+" set "+disciplina_transformada+" = '"+getResultado()+"' where periodo_semestre = "+getPeriodoAluno()+" and periodo_aluno_matricula = "+ getAlunoMatricula()+";";
				executa.executeUpdate(x);
			}
			
			
		}
		
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	private void adiciona_semestre (){
		Statement executa = null;
		Statement consulta = null;
		int semestre = 1;
		int periodo = 0;
		int matricula = 0;
		try{
			executa = conecta.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			consulta = conecta.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ResultSet alunos = consulta.executeQuery("select * from aluno_disciplina_transformada_"+getSiglaCurso()+" order by (periodo_aluno_matricula,periodo_semestre)");
			while(alunos.next()){
				if(alunos.isFirst()){
					executa.executeUpdate("update aluno_disciplina_transformada_"+getSiglaCurso()+" set semestre_letivo ="+semestre+" where periodo_aluno_matricula ="+alunos.getInt("periodo_aluno_matricula")+" " +
							" and periodo_semestre="+alunos.getInt("periodo_semestre")+";");		
					periodo = alunos.getInt("periodo_semestre");
					matricula = alunos.getInt("periodo_aluno_matricula");	
				}
				else if ((matricula == (alunos.getInt("periodo_aluno_matricula"))) && (periodo != (alunos.getInt("periodo_semestre")))){
					semestre++;
					executa.executeUpdate("update aluno_disciplina_transformada_"+getSiglaCurso()+" set semestre_letivo ="+semestre+" where periodo_aluno_matricula ="+alunos.getInt("periodo_aluno_matricula")+" " +
							"and periodo_semestre= "+alunos.getInt("periodo_semestre")+";");
					periodo = alunos.getInt("periodo_semestre");
					matricula = alunos.getInt("periodo_aluno_matricula");

				}
				else if ((matricula != (alunos.getInt("periodo_aluno_matricula"))) && (periodo != (alunos.getInt("periodo_semestre")))
						|| (matricula != (alunos.getInt("periodo_aluno_matricula"))) && (periodo == (alunos.getInt("periodo_semestre")))){
					semestre = 1;
					executa.executeUpdate("update aluno_disciplina_transformada_"+getSiglaCurso()+" set semestre_letivo ="+semestre+" where periodo_aluno_matricula ="+alunos.getInt("periodo_aluno_matricula")+" " +
							"and periodo_semestre= "+alunos.getInt("periodo_semestre")+";");
					periodo = alunos.getInt("periodo_semestre");
					matricula = alunos.getInt("periodo_aluno_matricula");
				}

			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void setDisciplinasCursadas(String disciplina){
		this.disciplinasCursadas[getPosicaoDisciplina()] = disciplina; 
	}
	
	private String getDisciplinasCursadas(int posicaoDisciplina){
		return disciplinasCursadas[getPosicaoDisciplina()];
	}
	
	private void setQuantidadeDisciplinasCursadas(){
		this.quantidadeDisciplinaCursadas[getPosicaoDisciplinaCursada()]++;
	}
	
	private int getQuantidadeDisciplinasCursadas(){
		return quantidadeDisciplinaCursadas[getPosicaoDisciplinaCursada()];
	}
	
	private void setMudouPeriodo(boolean mudouPeriodo){
		this.mudouPeriodo = mudouPeriodo;
	}
	
	private boolean getMudouPeriodo(){
		return mudouPeriodo;
	}
	
	private void setMudouAluno(boolean mudouAluno){
		this.mudouAluno = mudouAluno;
	}
	
	private boolean getMudouAluno(){
		return mudouAluno;
	}
	

	public int getCursoCodigo() {
		return cursoCodigo;
	}

	public void setCursoCodigo(int cursoCodigo) {
		this.cursoCodigo = cursoCodigo;
	}

	public int getCurriculoPeriodo() {
		return curriculoPeriodo;
	}

	public void setCurriculoPeriodo(int curriculoPeriodo) {
		this.curriculoPeriodo = curriculoPeriodo;
	}

	public String getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(String disciplina) {
		this.disciplina = disciplina;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	private void setConecta(Connect conecta){
		this.conecta = conecta;
	}
	
	private Connect getConecta(){
		return conecta;
	}

	public String getSiglaCurso() {
		return siglaCurso;
	}

	public void setSiglaCurso(String siglaCurso) {
		this.siglaCurso = siglaCurso;
	}
	
	private void setPosicaoDisciplina(int posicaoDisciplina){
		this.posicaoDisciplina = posicaoDisciplina;
	}
	private void incrementaPosicaoDisciplina(){
		this.posicaoDisciplina++;
	}
	
	private int getPosicaoDisciplina(){
		return posicaoDisciplina;
	}
	
	private void setPosicaoDisciplinaCursada(int posicaoDisciplinaCursada){
		this.posicaoDisciplinaCursada = posicaoDisciplinaCursada;
	}
	
	private int getPosicaoDisciplinaCursada(){
		return posicaoDisciplinaCursada;
	}
	
	private void setAlunoMatricula (int alunoMatricula){
		this.alunoMatricula = alunoMatricula;
	}
	
	private int getAlunoMatricula(){
		return alunoMatricula;
	}
	
	private void setPeriodoAluno(int periodoAluno){
		this.periodoAluno = periodoAluno;
	}
	
	private int getPeriodoAluno(){
		return periodoAluno;
	}
	
	
}
	
