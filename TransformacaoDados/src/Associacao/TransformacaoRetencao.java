package Associacao;
import java.sql.Statement;
import java.sql.ResultSet;

import javax.swing.JOptionPane;

import Conexao.Connect;


public class TransformacaoRetencao {
	private int semestreRelativo;
	private int cursoCodigo;
	private int alunoMatricula;
	private String disciplinasSemestre[]; //Disciplinas do semestre que têm pré-requisitos 
	private String requisitosDisciplina[][];
	private String siglaCurso;
	private int curriculoPeriodo;
	
	private Connect con;
	
	TransformacaoRetencao(){
		con = new Connect();
		setSemestreRelativo(2);
		setCursoCodigo(195140);
		this.disciplinasSemestre = new String[10];
		this.requisitosDisciplina = new String[10][10];
	}
	
	TransformacaoRetencao(String siglaCurso,int cursoCodigo, int curriculoPeriodo ){
		con = new Connect();
		con.conecta();
		setCursoCodigo(cursoCodigo);
		this.disciplinasSemestre = new String[10];
		this.requisitosDisciplina = new String[10][10];
		setCurriculoPeriodo(curriculoPeriodo);
		this.setSiglaCurso(siglaCurso);
	}
	


	public void retencaoRequisito(int semestreRelativo){
		Statement consulta;
		Statement consultaRetencao;
		int posicaoDisciplina = 0;
		int posicaoRequisito = 0;
		int retidos[] = new int[800];
		int naoRetidos[] = new int [800];
		int posicaoAlunosRetidos = 0;
		int posicaoAlunosNaoRetidos = 0;
 		boolean retido;
 		setSemestreRelativo(semestreRelativo);

		
 		
 		
 		
			//Limpa a lista de disciplinas antes de inserir as novas disciplinas do semestre posterior
	 		LimpaDisciplinas();
			
			//Insere as disciplinas do semestre selecionado
			//Insere os requisitos da disciplinas
			insereDisciplinasSemestre();
			
			try{
				consulta = con.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				consultaRetencao = con.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				//Consulta lista os alunos do curso selecionado, ordenando por alunos
				String str = "select distinct matricula from aluno join periodo on matricula = aluno_matricula " +
						" where curriculo_curso_codigo="+getCursoCodigo()+" and semestre_relativo="+getSemestreRelativo()+" " +
								" and curriculo_periodo_criado ="+getCurriculoPeriodo()+" order by matricula;";
				ResultSet aluno = consulta.executeQuery(str);
				while(aluno.next()){
					retido = false;
					setAlunoMatricula(aluno.getInt("matricula"));
					//Caso de testes
					if((getAlunoMatricula() == 200810635) && (semestreRelativo == 5)){
						String xxx = "dasda";
					}
					while(getDisciplinaSemestre(posicaoDisciplina)!=null){
						//Testa se o aluno esta cursando a disciplina
						if(!cursandoDisciplina(getDisciplinaSemestre(posicaoDisciplina))){
							//Se não estiver, percorre os requisitos desta disciplina para saber se ele foi aprovado anteriormente
							while(getRequisitoDisciplina(posicaoRequisito,posicaoDisciplina)!=null){
								//Se o aluno foi reprovado em pelo menos um requisito de alguma disciplina que ele nao pegou, ele será retido
								if(!aprovouNaDisciplina(getRequisitoDisciplina(posicaoRequisito,posicaoDisciplina))){
									//ALUNO RETIDO
									 retido = true;
									 str = "update aluno_disciplina_transformada_"+getSiglaCurso()+"" +
												" set retencao = true " +
												" where semestre_relativo="+getSemestreRelativo()+" and periodo_aluno_matricula="+getAlunoMatricula()+" ;";
									 consultaRetencao.execute(str);
									 retidos[posicaoAlunosRetidos] = getAlunoMatricula();
									 posicaoAlunosRetidos++;
									posicaoDisciplina = 0;
									posicaoRequisito = 0;
									break;
								}
								posicaoRequisito++;
							}
						}
						if(retido == true) break;
						
						posicaoDisciplina++;
						posicaoRequisito = 0;
					}
					if(retido == false){
						str = "update aluno_disciplina_transformada_"+getSiglaCurso()+"" +
								" set retencao = false " +
								" where semestre_relativo="+getSemestreRelativo()+" and periodo_aluno_matricula="+getAlunoMatricula()+" ;";
						consultaRetencao.execute(str);
						naoRetidos[posicaoAlunosNaoRetidos] = getAlunoMatricula();
						posicaoAlunosNaoRetidos++;
					}
					posicaoDisciplina = 0;
					posicaoRequisito = 0;
				}
				
			}
			catch (Exception e){
				e.printStackTrace();
			}
		System.out.println("ALUNOS RETIDOS");
		int cont = 0;
		for (int i =0; i<=posicaoAlunosRetidos; i++){
			System.out.println(retidos[i]);
			cont++;
		}
		System.out.println("Total Retidos: "+cont);
		System.out.println("*******************************************************************************");
		System.out.println("ALUNOS NÃO RETIDOS");
		cont = 0;
		for (int i =0; i<=posicaoAlunosNaoRetidos; i++){
			System.out.println(naoRetidos[i]);
			cont++;
		}
		System.out.println("Total Nao Ret: "+cont);
		System.out.println("TOTAL DE ALUNOS: "+(posicaoAlunosRetidos+posicaoAlunosNaoRetidos));	
	}

	private void LimpaDisciplinas() {
		//Limpa a lista de disciplinas antes de inserir as novas disciplinas do semestre posterior
 		for (int i = 0; i < disciplinasSemestre.length; i++){
 			disciplinasSemestre[i] = null;
 			for (int j = 0; j < disciplinasSemestre.length; j++){
 				requisitosDisciplina[i][j] = null;
 			}
 		}
	}
	
	private void retencaoCC(){
		//Ainda deve ser implementada
	}


	//Verifica se o aluno está cursando uma disciplina no semestre corrente
	private boolean cursandoDisciplina(String disciplina) {
		Statement executa;
		try{
			executa = con.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			//Consulta retorna a disciplina que o aluno cursou, ou null se não cursou
			String str = "select disciplina_codigo " +
					" from periodo_contem_disciplina join periodo on aluno_matricula = periodo_aluno_matricula and semestre = periodo_semestre " +
					" where semestre_relativo ="+getSemestreRelativo()+" and disciplina_codigo='"+disciplina+"' and periodo_aluno_matricula="+getAlunoMatricula()+";";
			ResultSet disciplinaBanco = executa.executeQuery(str);
			if(disciplinaBanco.next()){
				return true;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	


	//Este método verifica se o aluno já cursou uma disciplina em um semestre anterior ao corrente
	private boolean aprovouNaDisciplina(String disciplina) {
		Statement executa;
		try{
			executa = con.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			//Consulta retorna a disciplina que o aluno cursou, ou null se não cursou
			//MUDOU SQL
			String str = "select disciplina_codigo from periodo_contem_disciplina join periodo on aluno_matricula = periodo_aluno_matricula  and semestre = periodo_semestre" +
					" where disciplina_codigo='"+disciplina+"' and periodo_aluno_matricula="+getAlunoMatricula()+" " +
					" and semestre_relativo <"+getSemestreRelativo()+" and (resultado = 'DI' or resultado = 'DU' or resultado = 'AP' or resultado = 'AA' or resultado = 'AM' or resultado = 'MF');";
			ResultSet disciplinaBanco = executa.executeQuery(str);
			if(disciplinaBanco.next()){
				return true;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}

	//Esse método preenche o vetor das disciplinas do semestre que tem pre_requisito
	private void insereDisciplinasSemestre() {
		Statement executa;
		int posicao = 0;
		try{
			executa = con.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			//Consulta lista as disciplinas do semestre selecionado
			String str = "select disciplina_codigo from disciplina " +
					"join curriculo_contem_disciplina on codigo=disciplina_codigo and curriculo_curso_codigo ="+getCursoCodigo()+" and curriculo_periodo_criado="+getCurriculoPeriodo()+"" +
					" where semestre_recomendado ="+getSemestreRelativo()+";";
			ResultSet disciplinas = executa.executeQuery(str);
			while(disciplinas.next()){
				//Verifica se existe requisito para disciplina, caso seja verdade é inserido no array disciplinasSemestre
				String disciplinaAux = disciplinas.getString("disciplina_codigo"); 
				if(temRequisito(disciplinaAux,posicao)){
					disciplinasSemestre[posicao] = disciplinas.getString("disciplina_codigo");
					posicao++;
				}	
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}

	//Este método verifica se existe requisito para uma disciplina, se existir, insere os requisitos na matriz requisitosDisciplina
	private boolean temRequisito(String disciplina, int posicaoDisciplina){
		Statement executa;
		int posicaoRequisito = 0;
		try{
			executa = con.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			//retorna a disciplina e os seus pré requisitos
			String str = "select disciplina_codigo,requisito_codigo " +
					"from disciplina_contem_requisito " +
					"where disciplina_codigo='"+disciplina+"' and requisito_curso_codigo="+getCursoCodigo()+" and requisito_periodo_criado="+getCurriculoPeriodo();
			ResultSet requisito = executa.executeQuery(str);
			if(requisito.next()){
				//se a disciplina tem pré-requisitos deve-se inseri-los
				requisito.absolute(0);
				while(requisito.next()){
					requisitosDisciplina[posicaoRequisito][posicaoDisciplina] = requisito.getString("requisito_codigo");
					posicaoRequisito++;
				}
				return true;
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
private int posicaoDisciplina(String disciplina) {
		int posicao = 0;
		while(disciplinasSemestre[posicao]!=null){
			if(disciplinasSemestre[posicao] == disciplina){
				return posicao;
			}
		}
		//nunca vai ser retornado -1 
		return -1;
	}

	//--------------------------------------------GET E SETS DA CLASSE -----------------------------------------------------------------------------
	public int getSemestreRelativo() {
		return semestreRelativo;
	}

	public void setSemestreRelativo(int semestreRelativo) {
		this.semestreRelativo = semestreRelativo;
	}

	public String[] getDisciplinasSemestre() {
		return disciplinasSemestre;
	}

	public void setDisciplinasSemestre(String disciplinasSemestre[]) {
		this.disciplinasSemestre = disciplinasSemestre;
	}

	public Connect getCon(){
		return con;
	}

	private int getCursoCodigo() {
		return cursoCodigo;
	}

	private void setCursoCodigo(int cursoCodigo) {
		this.cursoCodigo = cursoCodigo;
	}

	private int getAlunoMatricula() {
		return alunoMatricula;
	}

	private void setAlunoMatricula(int alunoMatricula) {
		this.alunoMatricula = alunoMatricula;
	}
	
	private String getDisciplinaSemestre (int posicaoDisciplina){
		return disciplinasSemestre[posicaoDisciplina];
	}

	private String getRequisitoDisciplina(int posicaoRequisito, int posicaoDisciplina){
		return requisitosDisciplina[posicaoRequisito][posicaoDisciplina];
	}

	private int getCurriculoPeriodo() {
		return curriculoPeriodo;
	}

	private void setCurriculoPeriodo(int curriculoPeriodo) {
		this.curriculoPeriodo = curriculoPeriodo;
	}

	public String getSiglaCurso() {
		return siglaCurso;
	}

	public void setSiglaCurso(String siglaCurso) {
		this.siglaCurso = siglaCurso;
	}



}
