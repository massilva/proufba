package Associacao;

import java.sql.ResultSet;
import java.sql.Statement;

import Conexao.Connect;

public class GeraCSV {
	private Statement executa = null;
	private String siglaCurso;
	private Connect conexao;
	
	
	GeraCSV(String siglaCurso){
		this.siglaCurso = siglaCurso;
		conexao = new Connect();
	}
	
	public void geraArquivoCSV(int semestreRelativo){
		try{
			conexao.conecta();
			executa = conexao.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			String sql = "COPY (select * from aluno_disciplina_transformada_"+getSiglaCurso()+" where semestre_relativo ="+semestreRelativo+" )TO '/var/www/proufba/TransformacaoDados/input/semestre_"+semestreRelativo+".csv' DELIMITER ',' CSV HEADER;";
			executa.executeQuery(sql);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public String getSiglaCurso() {
		return siglaCurso;
	}

	public void setSiglaCurso(String siglaCurso) {
		this.siglaCurso = siglaCurso;
	}

}
