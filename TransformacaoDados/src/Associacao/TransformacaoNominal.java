package Associacao;
import java.sql.ResultSet;
import java.sql.Statement;

import Conexao.Connect;

public class TransformacaoNominal {

	private String siglaCurso;
	private int curriculoPeriodo;
	private int cursoCodigo;
	private Connect conecta;
	
	public TransformacaoNominal(String siglaCurso,int cursoCodigo, int curriculoPeriodo ) {
		setSiglaCurso(siglaCurso);
		setCurriculoPeriodo(curriculoPeriodo);
		setCursoCodigo(cursoCodigo);
		conecta = new Connect();
	}

	
	public void TransformaNominal(){
		if(conecta.conecta()){
			Statement executa = null;	
			Statement atualiza = null;
			try{
				executa = conecta.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				atualiza = conecta.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				String sql = " select a.attname as coluna from pg_catalog.pg_attribute a  inner join" +
						" pg_stat_user_tables c on a.attrelid = c.relid  " +
						"WHERE relname = 'aluno_disciplina_transformada_"+getSiglaCurso()+"' and a.attnum > 0 and a.attname!= 'periodo_aluno_matricula'" +
						" and a.attname!= 'periodo_semestre' and a.attname!= 'semestre_relativo' and attname!= 'retencao'" +
						" AND NOT a.attisdropped order by c.relname, a.attname ";
				ResultSet colunas = executa.executeQuery(sql);
				while(colunas.next()){
					String coluna = colunas.getString("coluna");
						atualiza.execute("begin;");
						sql = "update aluno_disciplina_transformada_"+getSiglaCurso()+" set "+coluna+" = 'APROVADO'" +
								" where "+coluna+"='DI' or "+coluna+"= 'DU' or "+coluna+"= 'AP' or "+coluna+"= 'AA' or "+coluna+"= 'AM' or "+coluna+"='MF' ";
						atualiza.execute(sql);
						sql = "update aluno_disciplina_transformada_"+getSiglaCurso()+" set "+coluna+" = 'REPROVADO_FALTA'" +
								" where "+coluna+"='RF' ";
						atualiza.execute(sql);
						sql = "update aluno_disciplina_transformada_"+getSiglaCurso()+" set "+coluna+" = 'REPROVADO'" +
								" where "+coluna+"='RC' or "+coluna+"='RM' or "+coluna+"='RR' or "+coluna+"='TR'";
						atualiza.execute(sql);
						sql = "update aluno_disciplina_transformada_"+getSiglaCurso()+" set "+coluna+" = NULL" +
								" where "+coluna+"='null'";
						atualiza.execute(sql);
				}
				atualiza.execute("commit;");
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}


	public int getCursoCodigo() {
		return cursoCodigo;
	}


	public void setCursoCodigo(int cursoCodigo) {
		this.cursoCodigo = cursoCodigo;
	}


	public int getCurriculoPeriodo() {
		return curriculoPeriodo;
	}


	public void setCurriculoPeriodo(int curriculoPeriodo) {
		this.curriculoPeriodo = curriculoPeriodo;
	}


	public String getSiglaCurso() {
		return siglaCurso;
	}


	public void setSiglaCurso(String siglaCurso) {
		this.siglaCurso = siglaCurso;
	}


	public Connect getConecta() {
		return conecta;
	}


	public void setConecta(Connect conecta) {
		this.conecta = conecta;
	}

}
