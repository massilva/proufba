package Associacao;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import Conexao.Connect;
import Essencial.TransformacaoPeriodo;


public class Apriori {

	public static void main(String[] args) throws SQLException {
		int cursoCodigo = Integer.parseInt((JOptionPane.showInputDialog("Digite o código do curso")));
 		int curriculoPeriodo = Integer.parseInt((JOptionPane.showInputDialog("Digite o periodo que a grade foi criada")));
 		String siglaCurso =  JOptionPane.showInputDialog("Digite a sigla do curso");
 		
 		//Transforma os periodos
 		TransformacaoPeriodo periodo = new TransformacaoPeriodo(cursoCodigo, curriculoPeriodo);
 		periodo.transformaPeriodo();

 		//Realiza a transformacao das disciplinas em colunas 
 		TransformacaoApriori apriori = new TransformacaoApriori(siglaCurso, cursoCodigo, curriculoPeriodo);
 		apriori.TransformaApriori();
 		
 		
 		//Adiciona a retencao por semestre
 		int ultimoSemestre = Integer.parseInt((JOptionPane.showInputDialog("Digite o ultimo periodo com disciplinas com requisitos")));
		//Adiciona +1 para pegar o ultimo semestre 
 		ultimoSemestre++;
 		TransformacaoRetencao retencao = new TransformacaoRetencao(siglaCurso,cursoCodigo,curriculoPeriodo);
			
			
		//Transforma para variavel Nominal
	 	TransformacaoNominal nominal = new TransformacaoNominal(siglaCurso,cursoCodigo,curriculoPeriodo);
	 	nominal.TransformaNominal();
			
		//Gera o CSV para ser aplicado 
		GeraCSV csv = new GeraCSV(siglaCurso);
		int semestre = 2;
 		while(semestre!=ultimoSemestre){
 			retencao.retencaoRequisito(semestre);
 			csv.geraArquivoCSV(semestre);
 			semestre++;
 		} 		
 				
	}

}
