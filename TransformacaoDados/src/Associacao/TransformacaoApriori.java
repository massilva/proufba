package Associacao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

import Conexao.Connect;



public class TransformacaoApriori {
	static Statement comando = null;
	static String disciplinas_cursadas[] = new String[100];
	static int quantidade_disciplina_cursada[] = new int[100];
	static Connect conecta;
	//SEMPRE MUDAR PARÂMETROS
	static String curso;
	static int codigo_curso;
	static int grade_curso; 
	
	TransformacaoApriori(String siglaCurso, int codigoCurso, int curriculoPeriodo){
		curso = siglaCurso;
		codigo_curso = codigoCurso;
		grade_curso = curriculoPeriodo;
		conecta = new Connect();
	}
	
	


	public void TransformaApriori() throws SQLException{
		if(conecta.conecta()){
			comando = conecta.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			deletaTabela();
			criaTabela();
			//Aqui deve inserir o código do curso
			
			//pega um aluno e trabalha com esse aluno ate ser um aluno diferente
			try{
				String str = "select periodo_semestre,periodo_aluno_matricula,disciplina_codigo,resultado,curriculo_curso_codigo " +
						"from periodo_contem_disciplina join aluno on periodo_aluno_matricula = matricula " +
						"where curriculo_curso_codigo = "+codigo_curso+" order by (periodo_aluno_matricula,periodo_semestre);";
				ResultSet dados_aluno = comando.executeQuery(str);
				if (dados_aluno != null){
					Arrays.fill ( quantidade_disciplina_cursada, 1);
					Arrays.fill ( disciplinas_cursadas, null);
					int posicao_disciplina = 0;
					int aluno_matricula = 0;
					int periodo_aluno = 0;
					int ultimo_aluno_matricula = 0;
					int ultimo_periodo_aluno = 0;
					boolean mudou_periodo = true; //variavel para identificar quando o aluno muda de periodo
					boolean mudou_aluno = false;
					String disciplina;
					String resultado;
					int posicao_disciplina_cursada;

				//	dados_aluno.absolute(1);
					while(dados_aluno.next()){
						if(dados_aluno.isFirst()){
							ultimo_aluno_matricula = dados_aluno.getInt("periodo_aluno_matricula");
							ultimo_periodo_aluno = dados_aluno.getInt("periodo_semestre");
							disciplina = dados_aluno.getString("disciplina_codigo");
							resultado = dados_aluno.getString("resultado");
							atualizaTabelaPrimeiro(ultimo_aluno_matricula, ultimo_periodo_aluno, disciplina, resultado, quantidade_disciplina_cursada[posicao_disciplina],mudou_periodo,mudou_aluno);
							mudou_periodo = false;
							disciplinas_cursadas[posicao_disciplina] = disciplina;
							posicao_disciplina++;
						}
						else{
							aluno_matricula = dados_aluno.getInt("periodo_aluno_matricula");
							periodo_aluno = dados_aluno.getInt("periodo_semestre");
							disciplina = dados_aluno.getString("disciplina_codigo");
							resultado = dados_aluno.getString("resultado");
							posicao_disciplina_cursada = ja_cursou_disciplina(disciplina);
							//Verifico se o aluno ja cursou a disciplina
							//Se cursou sera incrementado mais 1 na disciplina que ele cursou
							
							//Os alunos e periodos são iguais
							if((ultimo_periodo_aluno == periodo_aluno) && ultimo_aluno_matricula == aluno_matricula){	
								//Aluno ainda esta no mesmo semestre
								if  (posicao_disciplina_cursada != -1){
									quantidade_disciplina_cursada[posicao_disciplina_cursada]++;
									atualizaTabela(aluno_matricula, periodo_aluno, disciplina, resultado, quantidade_disciplina_cursada[posicao_disciplina_cursada],mudou_periodo,mudou_aluno);
									ultimo_aluno_matricula = aluno_matricula;
									ultimo_periodo_aluno = periodo_aluno;								
								}
								else{
									atualizaTabela(aluno_matricula, periodo_aluno, disciplina, resultado, quantidade_disciplina_cursada[posicao_disciplina],mudou_periodo,mudou_aluno);
									disciplinas_cursadas[posicao_disciplina] = disciplina;
									ultimo_aluno_matricula = aluno_matricula;
									ultimo_periodo_aluno = periodo_aluno;
									posicao_disciplina++;
								}
								
							}	
	
							//O aluno foi para o outro semestre
							else if((ultimo_periodo_aluno!=periodo_aluno) && ultimo_aluno_matricula == aluno_matricula){
								if  (posicao_disciplina_cursada != -1){
									mudou_periodo = true;					
									quantidade_disciplina_cursada[posicao_disciplina_cursada]++;
									atualizaTabela(aluno_matricula, periodo_aluno, disciplina, resultado, quantidade_disciplina_cursada[posicao_disciplina_cursada],mudou_periodo,mudou_aluno);
									ultimo_aluno_matricula = aluno_matricula;
									ultimo_periodo_aluno = periodo_aluno;								
									mudou_periodo = false;
								}
								else {
									mudou_periodo = true;					
									atualizaTabela(aluno_matricula, periodo_aluno, disciplina, resultado, quantidade_disciplina_cursada[posicao_disciplina],mudou_periodo,mudou_aluno);
									disciplinas_cursadas[posicao_disciplina] = disciplina;
									ultimo_aluno_matricula = aluno_matricula;
									ultimo_periodo_aluno = periodo_aluno;
									posicao_disciplina++;
									mudou_periodo = false;

								}
							}
							
							//O aluno mudou, logo o periodo tambem
							else{
								mudou_periodo = true;
								mudou_aluno = true;
								disciplina = dados_aluno.getString("disciplina_codigo");
								resultado = dados_aluno.getString("resultado");
								atualizaTabela(aluno_matricula, periodo_aluno, disciplina, resultado, quantidade_disciplina_cursada[posicao_disciplina],mudou_periodo,mudou_aluno);
								Arrays.fill ( quantidade_disciplina_cursada, 1);
								Arrays.fill ( disciplinas_cursadas, null);
								posicao_disciplina = 0;
								disciplinas_cursadas[posicao_disciplina] = disciplina;
								posicao_disciplina++;
								mudou_periodo = false;
								mudou_aluno = false;
								ultimo_aluno_matricula = aluno_matricula;
								ultimo_periodo_aluno = periodo_aluno;
								
								
							}
		
							//O período e o aluno ainda são os mesmos
												
						System.out.println(aluno_matricula);
						}
					}
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
			//adiciona o semestre dos alunos
			adiciona_semestre();
		}
	}
	
	private void cursou_disciplina (){
		
	}
	
	private int ja_cursou_disciplina(String disciplina){
		int posicao = 0;
		while(disciplinas_cursadas[posicao]!= null){
			if (disciplina.equals(disciplinas_cursadas[posicao])){
				return posicao;
			}
			posicao++;
		}
		return -1;
	}
	
	
	private void deletaTabela() {
		try{
			comando.executeUpdate("drop table aluno_disciplina_transformada_"+curso+";");
		}
		catch (Exception e){
			//Igonra erro, caso não exista a tabela
		}
		
	}


	private void criaTabela(){
		try{
			comando.executeUpdate("create table aluno_disciplina_transformada_"+curso+"( " +
					"periodo_semestre integer, " +
					"periodo_aluno_matricula integer," +
					"semestre_relativo integer," +
					"retencao boolean," +
					"primary key (periodo_aluno_matricula,periodo_semestre));");
		//	comando.executeUpdate("alter table aluno_disciplina_transformada_"+curso+" add primary key (periodo_semestre,periodo_aluno_matricula);");
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void atualizaTabela(int aluno_matricula, int periodo_aluno, String disciplina, String resultado, int quantidade_disciplina_cursada, boolean mudou_periodo, boolean mudou_aluno){
		Statement executa = null;
		boolean existe_disciplina = false;
		String disciplina_transformada = disciplina +"dis"+quantidade_disciplina_cursada;
		try{
			executa = conecta.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

			//Cria uma coluna da disciplina que o aluno cursou
			ResultSet colunas_existentes = executa.executeQuery("select a.attname as "+"coluna"+" from pg_catalog.pg_attribute a  inner join pg_stat_user_tables c on a.attrelid = c.relid WHERE relname = 'aluno_disciplina_transformada_"+curso+"' and a.attnum > 0 AND NOT a.attisdropped order by c.relname, a.attname");
			while(colunas_existentes.next()){
				String coluna = colunas_existentes.getString("coluna");
				if(coluna.equalsIgnoreCase(disciplina_transformada)){
					existe_disciplina = true;
					break;
				}
			}
			colunas_existentes.close();
			if(existe_disciplina != true)
				executa.executeUpdate("alter table aluno_disciplina_transformada_"+curso+" add column "+disciplina_transformada+" varchar(15);");
			//Adicionar chave primaria 
			//comando.executeUpdate("alter table aluno_disciplina_transformada_"+curso+" add primary key (periodo_semestre,periodo_aluno_matricula,"+
			//		disciplina +"dis"+quantidade_disciplina_cursada+");");
			if((mudou_periodo == true) && (mudou_aluno == true)){
				String x = "insert into aluno_disciplina_transformada_"+curso+" (periodo_semestre, periodo_aluno_matricula ) values ("+periodo_aluno+","+aluno_matricula+");";
				executa.executeUpdate(x);
				x = "update aluno_disciplina_transformada_"+curso+" set "+disciplina_transformada+" = '"+resultado+"' where periodo_semestre = "+periodo_aluno+" and periodo_aluno_matricula = "+ aluno_matricula+";";
				executa.executeUpdate(x);
			}
			else if (mudou_periodo == true){
				String x = "insert into aluno_disciplina_transformada_"+curso+" (periodo_semestre, periodo_aluno_matricula,"+disciplina +"dis"+quantidade_disciplina_cursada+") values ("+periodo_aluno+","+aluno_matricula+",'"+resultado+"');";
				executa.executeUpdate(x);
			}
			else{
				String x = "update aluno_disciplina_transformada_"+curso+" set "+disciplina_transformada+" = '"+resultado+"' where periodo_semestre = "+periodo_aluno+" and periodo_aluno_matricula = "+ aluno_matricula+";";
				executa.executeUpdate(x);
			}
			
			
		}
		
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void atualizaTabelaPrimeiro(int aluno_matricula, int periodo_aluno, String disciplina, String resultado, int quantidade_disciplina_cursada, boolean mudou_periodo, boolean mudou_aluno){
		Statement executa = null;
		boolean existe_disciplina = false;
		String disciplina_transformada = disciplina +"dis"+quantidade_disciplina_cursada;
		try{
			executa = conecta.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

			executa.executeUpdate("alter table aluno_disciplina_transformada_"+curso+" add column "+disciplina_transformada+" varchar(15);");

			String x = "insert into aluno_disciplina_transformada_"+curso+" (periodo_semestre, periodo_aluno_matricula,"+disciplina +"dis"+quantidade_disciplina_cursada+") values ("+periodo_aluno+","+aluno_matricula+",'"+resultado+"');";
			executa.executeUpdate(x);
			
			
		}
		
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void adiciona_semestre (){
		Statement executa = null;
		Statement consulta = null;
		int semestre = 1;
		int periodo = 0;
		int matricula = 0;
		
		try{
			executa = conecta.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			consulta = conecta.getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			//Retorna os alunos e seus respectivos periodos (2008-1,2008-2,2008-3, etc...)
			//ORDENADO POR ALUNO E PERIODO
			String sql = "select aluno_matricula,semestre from periodo join aluno on aluno_matricula = matricula " +
					"where curriculo_curso_codigo ="+codigo_curso+" and curriculo_periodo_criado ="+grade_curso+" "+
					"  order by aluno_matricula,semestre";
			
			ResultSet alunos = consulta.executeQuery(sql);
			while(alunos.next()){
				//Se for o primeiro aluno é atualizado o seu semestre para 1 e atualiza aluno e periodo
				if(alunos.isFirst()){
					sql = "update aluno_disciplina_transformada_"+curso+" set semestre_relativo ="+semestre+" where periodo_aluno_matricula ="+alunos.getInt("aluno_matricula")+" " +
							" and periodo_semestre="+alunos.getInt("semestre")+" ;";
					executa.executeUpdate(sql);
					matricula = alunos.getInt("aluno_matricula");	
					periodo = alunos.getInt("semestre");
					
				}
				//Se o aluno ainda é o mesmo e só mudou o periodo significa que ele passou para outro semestre.
				//Adiciona +1 na variável semestre, atualiza o aluno e periodo
				else if ((matricula == (alunos.getInt("aluno_matricula"))) && (periodo != (alunos.getInt("semestre")))){
					semestre++;
					sql = "update aluno_disciplina_transformada_"+curso+" set semestre_relativo ="+semestre+" where periodo_aluno_matricula ="+alunos.getInt("aluno_matricula")+" " +
							" and periodo_semestre="+alunos.getInt("semestre")+" ;";
					executa.executeUpdate(sql);
					matricula = alunos.getInt("aluno_matricula");
					periodo = alunos.getInt("semestre");

				}
				//Se mudou o aluno, esse novo aluno está no primeiro semestre, pois a consulta esta ordenada por aluno e periodo
				else if (  (matricula != (alunos.getInt("aluno_matricula")) && periodo != (alunos.getInt("semestre"))) ||
						(matricula != (alunos.getInt("aluno_matricula")) && periodo == (alunos.getInt("semestre"))) ) {							
					semestre = 1;
					sql = "update aluno_disciplina_transformada_"+curso+" set semestre_relativo ="+semestre+" where periodo_aluno_matricula ="+alunos.getInt("aluno_matricula")+" " +
							" and periodo_semestre="+alunos.getInt("semestre")+" ;";
					executa.executeUpdate(sql);
					matricula = alunos.getInt("aluno_matricula");
					periodo = alunos.getInt("semestre");
				}

			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
	
