package Essencial;
/*
 * @author Marcelo Silva
 * @last modification 01/07
 * @object Adicionar a variavel semestre_relativo para os alunos que se matricularam em disciplinas em um determinado periodo da sua vida acadêmica 
 * @logic É listado para cada aluno um periodo que ele se matriculou em disciplinas, ordenado pela matricula do aluno e periodo
 * Desta forma é realizada uma varredura em cada aluno e periodo atualizando o seu semestre. 
 * EX: Se um aluno aparece repetidamente em 4 linhas, na primeira linha será atualizado o semestre 1, na segunda linha semestre 2 até a última
 * linha que será atribuido o semestre 4. 
 * Quando o aluno é modificado o processo repete-se para esse novo aluno.
 * Esse processo é repetido até todos os semestres estejam atualizados
 */

//Funcionando Perfeitamente

import java.sql.ResultSet;
import java.sql.Statement;

import Conexao.Connect;



public class TransformacaoPeriodo {
	private int cursoCodigo;
	private int curriculoPeriodo;
	private Connect conecta;

	
	
	public TransformacaoPeriodo(int cursoCodigo, int curriculoPeriodo){
		setCurriculoPeriodo(curriculoPeriodo);
		setCursoCodigo(cursoCodigo);
		conecta = new Connect();
		conecta.conecta();

	}
	
	public void transformaPeriodo(){
		Statement executaUpdate = null;
		Statement consultaAluno = null;
		int semestre = 1;
		int periodo = 0;
		int matricula = 0;
		
		try{
			executaUpdate = getConecta().getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			consultaAluno = getConecta().getCon().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			//Retorna os alunos e seus respectivos periodos (2008-1,2008-2,2008-3, etc...)
			//ORDENADO POR ALUNO E PERIODO
			String sql = "select aluno_matricula,semestre from periodo join aluno on aluno_matricula = matricula " +
					"where curriculo_curso_codigo ="+getCursoCodigo()+" and curriculo_periodo_criado ="+getCurriculoPeriodo()+" "+
					"  order by aluno_matricula,semestre";
			
			ResultSet alunos = consultaAluno.executeQuery(sql);
			while(alunos.next()){
				//Se for o primeiro aluno é atualizado o seu semestre para 1 e atualiza aluno e periodo
				if(alunos.isFirst()){
					sql = "update periodo set semestre_relativo ="+semestre+" where aluno_matricula ="+alunos.getInt("aluno_matricula")+" " +
							" and semestre="+alunos.getInt("semestre")+" ;";
					executaUpdate.executeUpdate(sql);
					matricula = alunos.getInt("aluno_matricula");	
					periodo = alunos.getInt("semestre");
					
				}
				//Se o aluno ainda é o mesmo e só mudou o periodo significa que ele passou para outro semestre.
				//Adiciona +1 na variável semestre, atualiza o aluno e periodo
				else if ((matricula == (alunos.getInt("aluno_matricula"))) && (periodo != (alunos.getInt("semestre")))){
					semestre++;
					sql = "update periodo set semestre_relativo ="+semestre+" where aluno_matricula ="+alunos.getInt("aluno_matricula")+" " +
							" and semestre="+alunos.getInt("semestre")+" ;";
					executaUpdate.executeUpdate(sql);
					matricula = alunos.getInt("aluno_matricula");
					periodo = alunos.getInt("semestre");

				}
				//Se mudou o aluno, esse novo aluno está no primeiro semestre, pois a consulta esta ordenada por aluno e periodo
				else if (  (matricula != (alunos.getInt("aluno_matricula")) && periodo != (alunos.getInt("semestre"))) ||
						(matricula != (alunos.getInt("aluno_matricula")) && periodo == (alunos.getInt("semestre"))) ) {							
					semestre = 1;
					sql = "update periodo set semestre_relativo ="+semestre+" where aluno_matricula ="+alunos.getInt("aluno_matricula")+" " +
							" and semestre="+alunos.getInt("semestre")+" ;";
					executaUpdate.executeUpdate(sql);
					matricula = alunos.getInt("aluno_matricula");
					periodo = alunos.getInt("semestre");
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("Dados atualizados com suceso");
		
	}
	
	
	private int getCursoCodigo() {
		return cursoCodigo;
	}

	private void setCursoCodigo(int cursoCodigo) {
		this.cursoCodigo = cursoCodigo;
	}
	
	private int getCurriculoPeriodo() {
		return curriculoPeriodo;
	}

	private void setCurriculoPeriodo(int curriculoPeriodo) {
		this.curriculoPeriodo = curriculoPeriodo;
	}
	
	private void setConecta(Connect conecta){
		this.conecta = conecta;
	}
	
	private Connect getConecta(){
		return conecta;
	}
	
}
