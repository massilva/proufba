package Conexao;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {
    Connection con;
	String url = "jdbc:postgresql://localhost:5432/proufba";
	String usuario = "postgres";
	String senha = "123";
	
	public Connect(){}

	public boolean conecta(){
		try {
			Class.forName("org.postgresql.Driver");			
			con = DriverManager.getConnection(url, usuario, senha);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean desconecta(){
		try {
			con.close();
			return true;
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}
		
	}
	
	public Connection getCon(){
		return con;
	}
}
